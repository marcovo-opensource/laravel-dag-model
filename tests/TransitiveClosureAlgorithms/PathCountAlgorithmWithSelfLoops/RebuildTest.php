<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithmWithSelfLoops;

use Marcovo\LaravelDagModel\Tests\fixtures\SituationPathCountAlgorithmSelfLoopsEdge;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;

class RebuildTest extends \Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithm\RebuildTest
{
    protected bool $checkSelfLoops = true;

    protected function getAlgorithm(): PathCountAlgorithm
    {
        return new PathCountAlgorithm(new SituationPathCountAlgorithmSelfLoopsEdge());
    }
}
