<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithmWithSelfLoops;

use Marcovo\LaravelDagModel\Tests\fixtures\SituationPathCountAlgorithmSelfLoopsEdge;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;

class GetTest extends \Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithm\GetTest
{
    protected bool $checkSelfLoops = true;

    protected function getAlgorithm(): PathCountAlgorithm
    {
        return new PathCountAlgorithm(new SituationPathCountAlgorithmSelfLoopsEdge());
    }
}
