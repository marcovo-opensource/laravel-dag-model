<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\ForestAlgorithmWithSelfLoops;

use Marcovo\LaravelDagModel\Tests\fixtures\SituationForestAlgorithmSelfLoopsEdge;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\ForestAlgorithm;

class GetTest extends \Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\ForestAlgorithm\GetTest
{
    protected bool $checkSelfLoops = true;

    protected function getAlgorithm(): ForestAlgorithm
    {
        return new ForestAlgorithm(new SituationForestAlgorithmSelfLoopsEdge());
    }
}
