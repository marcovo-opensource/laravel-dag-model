<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\DlswAlgorithm;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\CycleException;
use Marcovo\LaravelDagModel\Exceptions\DuplicateEdgeException;
use Marcovo\LaravelDagModel\Exceptions\SelfLinkException;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationDlswAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\DlswAlgorithm;
use PHPUnit\Framework\Attributes\DataProvider;

class CreateTest extends TestCase
{
    protected function getAlgorithm(): DlswAlgorithm
    {
        return new DlswAlgorithm(new SituationDlswAlgorithmEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_dlsw_algorithm_edge')->get();
    }

    public function test_can_create_an_edge()
    {
        $this->withVertices($V = [1, 2]);

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2]
            ],
            [],
            $V,
            $this->getAllEdges()
        );
    }

    public function test_can_create_chain_of_two_edges()
    {
        $this->withVertices($V = [1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);


        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
            ],
            [
                [1, 3],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    public function test_cannot_add_edge_from_vertex_to_itself()
    {
        $this->withVertices([1]);

        $this->expectException(SelfLinkException::class);
        $this->getAlgorithm()->createEdge(1, 1);
    }

    public function test_cannot_add_edge_twice()
    {
        $this->withVertices([1, 2]);

        $this->getAlgorithm()->createEdge(1, 2);

        $this->expectException(DuplicateEdgeException::class);
        $this->getAlgorithm()->createEdge(1, 2);
    }

    public function test_does_not_allow_direct_cycles()
    {
        $this->withVertices([1, 2]);

        $this->getAlgorithm()->createEdge(1, 2);

        $this->expectException(CycleException::class);
        $this->getAlgorithm()->createEdge(2, 1);
    }

    public function test_does_not_allow_indirect_cycles()
    {
        $this->withVertices([1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->expectException(CycleException::class);
        $this->getAlgorithm()->createEdge(3, 1);
    }

    /**
     * Tests creation of chain in order (1, 2), (2, 3), (3, 4):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
            ],
            [
                [1, 3],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of chain in order (3, 4), (2, 3), (1, 2):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges_reverse()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [3, 4]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [2, 3],
                [3, 4],
            ],
            [
                [2, 4],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of chain in order (2, 3), (1, 2), (3, 4):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges_inside_out()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [2, 3]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
            ],
            [
                [1, 3],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of chain in order (1, 2), (3, 4), (2, 3):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges_outside_in()
    {
        $this->withVertices($V =[1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [3, 4],
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of diamond by adding (3, 4) last
     *       1
     *      / \
     *     2   3
     *      \ /
     *       4
     */
    public function test_can_create_diamond_closing_at_bottom()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [1, 3],
            ],
            [
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [1, 3],
                [3, 4],
            ],
            [
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of diamond by adding (1, 3) last
     *       1
     *      / \
     *     2   3
     *      \ /
     *       4
     */
    public function test_can_create_diamond_closing_at_top()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [3, 4],
            ],
            [
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [1, 3],
                [3, 4],
            ],
            [
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of box diamond by adding (5, 6) last
     *       1
     *      / \
     *     2   3
     *     |   |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_create_box_diamond_closing_at_bottom()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [4, 6],
                [1, 3],
                [3, 5],
            ],
            [
                [1, 4],
                [2, 6],
                [1, 5],
                [1, 6],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [4, 6],
                [1, 3],
                [3, 5],
                [5, 6],
            ],
            [
                [1, 4],
                [2, 6],
                [1, 5],
                [3, 6],
                [1, 6],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of box diamond by adding (1, 3) last
     *       1
     *      / \
     *     2   3
     *     |   |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_create_box_diamond_closing_at_top()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [4, 6],
                [3, 5],
                [5, 6],
            ],
            [
                [1, 4],
                [2, 6],
                [3, 6],
                [1, 6],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [4, 6],
                [1, 3],
                [3, 5],
                [5, 6],
            ],
            [
                [1, 4],
                [2, 6],
                [1, 5],
                [3, 6],
                [1, 6],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of box diamond by adding (3, 5) last
     *       1
     *      / \
     *     2   3
     *     |   |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_create_box_diamond_closing_in_middle()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [4, 6],
                [1, 3],
                [5, 6],
            ],
            [
                [1, 4],
                [2, 6],
                [1, 6],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 5);

        $this->assertEdges(
            [
                [1, 2],
                [2, 4],
                [4, 6],
                [1, 3],
                [3, 5],
                [5, 6],
            ],
            [
                [1, 4],
                [2, 6],
                [1, 5],
                [3, 6],
                [1, 6],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of tri-diamond by adding (4, 5) last
     *       1
     *      /|\
     *     2 3 4
     *      \|/
     *       5
     */
    public function test_can_create_tridiamond_closing_at_bottom()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 5);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(1, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 5],
                [1, 3],
                [3, 5],
                [1, 4],
            ],
            [
                [1, 5],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(4, 5);

        $this->assertEdges(
            [
                [1, 2],
                [2, 5],
                [1, 3],
                [3, 5],
                [1, 4],
                [4, 5],
            ],
            [
                [1, 5],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of tri-diamond by adding (1, 4) last
     *       1
     *      /|\
     *     2 3 4
     *      \|/
     *       5
     */
    public function test_can_create_tridiamond_closing_at_top()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 5);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(4, 5);

        $this->assertEdges(
            [
                [1, 2],
                [2, 5],
                [1, 3],
                [3, 5],
                [4, 5],
            ],
            [
                [1, 5],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 5],
                [1, 3],
                [3, 5],
                [1, 4],
                [4, 5],
            ],
            [
                [1, 5],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    public static function provider_can_create_diagonal_diamond()
    {
        return [
            [0], [1], [2], [3], [4], [5], [6],
        ];
    }

    /**
     * Tests creation of diagonal diamond by switching which of the 7 edges to add last
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     * @dataProvider provider_can_create_diagonal_diamond
     */
    #[DataProvider('provider_can_create_diagonal_diamond')]
    public function test_can_create_diagonal_diamond(int $skipIndex)
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $vertices = [
            [1, 2],
            [1, 3],
            [2, 4],
            [2, 5],
            [3, 5],
            [4, 6],
            [5, 6],
        ];

        for ($i = 0; $i < 7; $i++) {
            if ($i !== $skipIndex) {
                $this->getAlgorithm()->createEdge($vertices[$i][0], $vertices[$i][1]);
            }
        }

        $this->getAlgorithm()->createEdge($vertices[$skipIndex][0], $vertices[$skipIndex][1]);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [2, 4],
                [2, 5],
                [3, 5],
                [4, 6],
                [5, 6],
            ],
            [
                [1, 4],
                [2, 6],
                [1, 5],
                [3, 6],
                [1, 6],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of edge (2, 4) after already being in the transitive closure
     *         1
     *        /
     *       2
     *      / \
     *     3 - 4
     */
    public function test_can_create_edge_that_already_was_in_transitive_closure()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
                [2, 4],
            ],
            [
                [1, 3],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests whether created_at and updated_at are set, on a diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     */
    public function test_sets_timestamps()
    {
        $this->withVertices([1, 2, 3, 4, 5, 6]);

        $vertices = [
            [1, 2],
            [1, 3],
            [2, 4],
            [2, 5],
            [3, 5],
            [4, 6],
            [5, 6],
        ];

        for ($i = 0; $i < 7; $i++) {
            $this->getAlgorithm()->createEdge($vertices[$i][0], $vertices[$i][1]);
        }

        foreach ($this->getAllEdges() as $edge) {
            $this->assertNotNull($edge->created_at);
            $this->assertNotNull($edge->updated_at);
        }

        DB::table('situation_dlsw_algorithm_edge')->update(['updated_at' => null]);

        $this->getAlgorithm()->createEdge(5, 7);
        $this->assertNull(
            $this->getAllEdges()
                ->where('start_vertex', 5)
                ->where('end_vertex', 6)
                ->first()->updated_at
        );

        $this->getAlgorithm()->createEdge(7, 6);
        $this->assertNotNull(
            $this->getAllEdges()
                ->where('start_vertex', 5)
                ->where('end_vertex', 6)
                ->first()->updated_at
        );
    }
}
