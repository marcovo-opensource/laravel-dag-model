<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\DlswAlgorithmWithSelfLoops;

use Marcovo\LaravelDagModel\Tests\fixtures\SituationDlswAlgorithmSelfLoopsEdge;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\DlswAlgorithm;

class GetTest extends \Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\DlswAlgorithm\GetTest
{
    protected bool $checkSelfLoops = true;

    protected function getAlgorithm(): DlswAlgorithm
    {
        return new DlswAlgorithm(new SituationDlswAlgorithmSelfLoopsEdge());
    }
}
