<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\ForestAlgorithm;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\EdgeNotFoundException;
use Marcovo\LaravelDagModel\Exceptions\SeparationException;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationForestAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\ForestAlgorithm;

class SeparationTest extends TestCase
{
    private function getForestAlgorithm(\Closure $separationCallback): ForestAlgorithm
    {
        $forestAlgorithm = new ForestAlgorithm(new SituationForestAlgorithmEdge());
        $forestAlgorithm->setSeparationCallback($separationCallback);
        return $forestAlgorithm;
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_forest_algorithm_edge')->get();
    }

    /**
     * Tests whether we can build two separated forests
     *       1        6
     *      / \      / \
     *     2   3    7   8
     *        /      \
     *       4        9
     */
    public function test_can_build_separated_forests()
    {
        $pc1 = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $pc2 = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '>', 5);
        });

        $pc1->createEdge(1, 2);
        $pc1->createEdge(1, 3);
        $pc1->createEdge(3, 4);

        $pc2->createEdge(6, 7);
        $pc2->createEdge(7, 9);
        $pc2->createEdge(6, 8);

        $edges = $this->getAllEdges()->groupBy(fn ($edge) => ($edge->start_vertex < 5) ? 1 : 2);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [3, 4],
            ],
            [
                [1, 4],
            ],
            [],
            $edges[1]
        );

        $this->assertEdges(
            [
                [6, 7],
                [7, 9],
                [6, 8],
            ],
            [
                [6, 9],
            ],
            [],
            $edges[2]
        );
    }

    /**
     * Tests whether we can remove edges (3, 4) and (6, 8)
     *       1        6
     *      / \      / \
     *     2   3    7   8
     *        /      \
     *       4        9
     */
    public function test_can_remove_edges()
    {
        $pc1 = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $pc2 = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '>', 5);
        });

        $pc1->createEdge(1, 2);
        $pc1->createEdge(1, 3);
        $pc1->createEdge(3, 4);

        $pc2->createEdge(6, 7);
        $pc2->createEdge(7, 9);
        $pc2->createEdge(6, 8);

        $edges = $this->getAllEdges()->groupBy(fn ($edge) => ($edge->start_vertex < 5) ? 1 : 2);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [3, 4],
            ],
            [
                [1, 4],
            ],
            [],
            $edges[1]
        );

        $this->assertEdges(
            [
                [6, 7],
                [7, 9],
                [6, 8],
            ],
            [
                [6, 9],
            ],
            [],
            $edges[2]
        );

        $pc1->deleteEdge(3, 4);

        $edges = $this->getAllEdges()->groupBy(fn ($edge) => ($edge->start_vertex < 5) ? 1 : 2);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
            ],
            [
            ],
            [],
            $edges[1]
        );

        $this->assertEdges(
            [
                [6, 7],
                [7, 9],
                [6, 8],
            ],
            [
                [6, 9],
            ],
            [],
            $edges[2]
        );

        $pc2->deleteEdge(6, 8);

        $edges = $this->getAllEdges()->groupBy(fn ($edge) => ($edge->start_vertex < 5) ? 1 : 2);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
            ],
            [
            ],
            [],
            $edges[1]
        );

        $this->assertEdges(
            [
                [6, 7],
                [7, 9],
            ],
            [
                [6, 9],
            ],
            [],
            $edges[2]
        );
    }

    public function test_cannot_add_edge_between_separated_forests()
    {
        $pc = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $this->expectException(SeparationException::class);
        $pc->createEdge(4, 6);
    }

    public function test_cannot_add_edge_in_other_forest()
    {
        $pc = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $this->expectException(SeparationException::class);
        $pc->createEdge(6, 7);
    }

    public function test_cannot_delete_edge_in_other_forest()
    {
        $pc1 = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $pc2 = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '>', 5);
        });

        $pc2->createEdge(6, 7);

        $this->expectException(EdgeNotFoundException::class);
        $pc1->deleteEdge(6, 7);
    }

    public function test_cannot_get_edge_from_another_forest()
    {
        $pc1 = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $pc2 = $this->getForestAlgorithm(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '>', 5);
        });

        $pc2->createEdge(6, 7);

        $this->assertNull($pc1->getEdge(6, 7));
        $this->assertNull($pc1->getGraphEdge(6, 7));
        $this->assertFalse($pc1->hasEdge(6, 7));
        $this->assertFalse($pc1->hasGraphEdge(6, 7));
    }
}
