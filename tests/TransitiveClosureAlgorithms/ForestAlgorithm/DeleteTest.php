<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\ForestAlgorithm;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\EdgeNotFoundException;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationForestAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\ForestAlgorithm;

class DeleteTest extends TestCase
{
    protected function getAlgorithm(): ForestAlgorithm
    {
        return new ForestAlgorithm(new SituationForestAlgorithmEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_forest_algorithm_edge')->get();
    }

    public function test_can_delete_an_edge()
    {
        $this->withVertices($V = [1, 2]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2],
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(1, 2);

        $this->assertEdges([], [], $V, $this->getAllEdges());
    }

    public function test_can_delete_edge_from_chain_of_two_edges()
    {
        $this->withVertices($V = [1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
            ],
            [
                [1, 3],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(1, 2);

        $this->assertEdges(
            [
                [2, 3],
            ],
            [],
            $V,
            $this->getAllEdges()
        );
    }

    public function test_cannot_delete_non_existing_edge()
    {
        $this->withVertices([1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->expectException(EdgeNotFoundException::class);
        $this->getAlgorithm()->deleteEdge(1, 3);
    }

    /**
     * Tests deleting edge (1, 2) from a chain of three edges:
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_delete_first_edge_in_chain_of_three_edges()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(1, 2);

        $this->assertEdges(
            [
                [2, 3],
                [3, 4],
            ],
            [
                [2, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (2, 3) from a chain of three edges:
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_delete_second_edge_in_chain_of_three_edges()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2],
                [3, 4],
            ],
            [],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (3, 4) from a chain of three edges:
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_delete_last_edge_in_chain_of_three_edges()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
            ],
            [
                [1, 3],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests detaching one tree from another tree
     *      1
     *     / \
     *    2   3
     *         X
     *          4
     *         / \
     *        5   6
     */
    public function test_can_detach_one_tree_from_another()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(4, 5);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [4, 5],
                [4, 6],
                [3, 4],
            ],
            [
                [1, 4],
                [1, 5],
                [1, 6],
                [3, 5],
                [3, 6],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [4, 5],
                [4, 6],
            ],
            [
            ],
            $V,
            $this->getAllEdges()
        );
    }
}
