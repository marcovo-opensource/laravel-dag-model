<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\ForestAlgorithm;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\CycleException;
use Marcovo\LaravelDagModel\Exceptions\DuplicateEdgeException;
use Marcovo\LaravelDagModel\Exceptions\InDegreeException;
use Marcovo\LaravelDagModel\Exceptions\SelfLinkException;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationForestAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\ForestAlgorithm;

class CreateTest extends TestCase
{
    protected function getAlgorithm(): ForestAlgorithm
    {
        return new ForestAlgorithm(new SituationForestAlgorithmEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_forest_algorithm_edge')->get();
    }

    public function test_can_create_an_edge()
    {
        $this->withVertices($V = [1, 2]);

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2]
            ],
            [],
            $V,
            $this->getAllEdges()
        );
    }

    public function test_can_create_chain_of_two_edges()
    {
        $this->withVertices($V = [1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);


        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
            ],
            [
                [1, 3],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    public function test_cannot_add_edge_from_vertex_to_itself()
    {
        $this->withVertices([1]);

        $this->expectException(SelfLinkException::class);
        $this->getAlgorithm()->createEdge(1, 1);
    }

    public function test_cannot_add_edge_twice()
    {
        $this->withVertices([1, 2]);

        $this->getAlgorithm()->createEdge(1, 2);

        $this->expectException(DuplicateEdgeException::class);
        $this->getAlgorithm()->createEdge(1, 2);
    }

    public function test_does_not_allow_direct_cycles()
    {
        $this->withVertices([1, 2]);

        $this->getAlgorithm()->createEdge(1, 2);

        $this->expectException(CycleException::class);
        $this->getAlgorithm()->createEdge(2, 1);
    }

    public function test_does_not_allow_indirect_cycles()
    {
        $this->withVertices([1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->expectException(CycleException::class);
        $this->getAlgorithm()->createEdge(3, 1);
    }

    public function test_does_not_allow_two_parents()
    {
        $this->getAlgorithm()->createEdge(1, 2);

        $this->expectException(InDegreeException::class);
        $this->getAlgorithm()->createEdge(3, 2);
    }

    public function test_can_not_add_edge_that_was_in_transitive_closure()
    {
        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->expectException(InDegreeException::class);
        $this->getAlgorithm()->createEdge(1, 3);
    }

    /**
     * Tests creation of chain in order (1, 2), (2, 3), (3, 4):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
            ],
            [
                [1, 3],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of chain in order (3, 4), (2, 3), (1, 2):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges_reverse()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [3, 4]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [2, 3],
                [3, 4],
            ],
            [
                [2, 4],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of chain in order (2, 3), (1, 2), (3, 4):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges_inside_out()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [2, 3]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
            ],
            [
                [1, 3],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of chain in order (1, 2), (3, 4), (2, 3):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges_outside_in()
    {
        $this->withVertices($V =[1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [3, 4],
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
                [3, 4],
            ],
            [
                [1, 3],
                [2, 4],
                [1, 4],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of simple tree
     *      1
     *     / \
     *    2   3
     */
    public function test_vertex_can_have_multiple_children()
    {
        $this->withVertices($V = [1, 2, 3]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
            ],
            [
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of larger tree
     *      1
     *     / \
     *    2   3
     *   / \ / \
     *  4  5 6  7
     */
    public function test_can_build_tree_of_two_layers()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6, 7]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
            ],
            [
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(2, 5);
        $this->getAlgorithm()->createEdge(3, 6);
        $this->getAlgorithm()->createEdge(3, 7);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [2, 4],
                [2, 5],
                [3, 6],
                [3, 7],
            ],
            [
                [1, 4],
                [1, 5],
                [1, 6],
                [1, 7],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests attaching one tree to another tree
     *      1
     *     / \
     *    2   3
     *         X
     *          4
     *         / \
     *        5   6
     */
    public function test_can_attach_one_tree_to_another()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(4, 5);
        $this->getAlgorithm()->createEdge(4, 6);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [4, 5],
                [4, 6],
            ],
            [
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [4, 5],
                [4, 6],
                [3, 4],
            ],
            [
                [1, 4],
                [1, 5],
                [1, 6],
                [3, 5],
                [3, 6],
            ],
            $V,
            $this->getAllEdges()
        );
    }
}
