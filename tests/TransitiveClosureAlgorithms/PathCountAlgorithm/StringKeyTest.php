<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithm;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationPathCountAlgorithmStringEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;

class StringKeyTest extends TestCase
{
    private function getPathCount(): PathCountAlgorithm
    {
        return new PathCountAlgorithm(new SituationPathCountAlgorithmStringEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_path_count_algorithm_string_edge')->get();
    }

    /**
     * Create diagonal diamond
     *       A
     *      / \
     *     B   C
     *     | \ |
     *     D   E
     *      \ /
     *       F
     */
    private function createDiagonalDiamond()
    {
        $vertices = [
            ['A', 'B'],
            ['A', 'C'],
            ['B', 'D'],
            ['B', 'E'],
            ['C', 'E'],
            ['D', 'F'],
            ['E', 'F'],
        ];

        for ($i = 0; $i < 7; $i++) {
            $this->getPathCount()->createEdge($vertices[$i][0], $vertices[$i][1]);
        }
    }

    public function test_can_create_graph_with_string_vertices()
    {
        $this->createDiagonalDiamond();

        $this->assertEdges(
            [
                ['A', 'B', 'path_count' => 1],
                ['A', 'C', 'path_count' => 1],
                ['B', 'D', 'path_count' => 1],
                ['B', 'E', 'path_count' => 1],
                ['C', 'E', 'path_count' => 1],
                ['D', 'F', 'path_count' => 1],
                ['E', 'F', 'path_count' => 1],
            ],
            [
                ['A', 'D', 'path_count' => 1],
                ['B', 'F', 'path_count' => 2],
                ['A', 'E', 'path_count' => 2],
                ['C', 'F', 'path_count' => 1],
                ['A', 'F', 'path_count' => 3],
            ],
            [],
            $this->getAllEdges()
        );
    }

    public function test_can_delete_edge()
    {
        $this->createDiagonalDiamond();

        $this->getPathCount()->deleteEdge('C', 'E');

        $this->assertEdges(
            [
                ['A', 'B', 'path_count' => 1],
                ['A', 'C', 'path_count' => 1],
                ['B', 'D', 'path_count' => 1],
                ['B', 'E', 'path_count' => 1],
                ['D', 'F', 'path_count' => 1],
                ['E', 'F', 'path_count' => 1],
            ],
            [
                ['A', 'D', 'path_count' => 1],
                ['B', 'F', 'path_count' => 2],
                ['A', 'E', 'path_count' => 1],
                ['A', 'F', 'path_count' => 2],
            ],
            [],
            $this->getAllEdges()
        );

        $this->getPathCount()->deleteEdge('B', 'D');

        $this->assertEdges(
            [
                ['A', 'B', 'path_count' => 1],
                ['A', 'C', 'path_count' => 1],
                ['B', 'E', 'path_count' => 1],
                ['D', 'F', 'path_count' => 1],
                ['E', 'F', 'path_count' => 1],
            ],
            [
                ['B', 'F', 'path_count' => 1],
                ['A', 'E', 'path_count' => 1],
                ['A', 'F', 'path_count' => 1],
            ],
            [],
            $this->getAllEdges()
        );
    }

    public function test_can_determine_has_a_graph_edge()
    {
        $this->getPathCount()->createEdge('A', 'B');

        $this->assertTrue($this->getPathCount()->hasGraphEdge('A', 'B'));
        $this->assertTrue($this->getPathCount()->hasEdge('A', 'B'));
    }

    public function test_can_determine_has_a_transitive_closure_edge()
    {
        $this->getPathCount()->createEdge('A', 'B');
        $this->getPathCount()->createEdge('B', 'C');

        $this->assertFalse($this->getPathCount()->hasGraphEdge('A', 'C'));
        $this->assertTrue($this->getPathCount()->hasEdge('A', 'C'));
    }

    public function test_can_get_a_graph_edge()
    {
        $this->getPathCount()->createEdge('A', 'B');

        $this->assertNotNull($edge1 = $this->getPathCount()->getGraphEdge('A', 'B'));
        $this->assertSame('A', $edge1->start_vertex);
        $this->assertSame('B', $edge1->end_vertex);

        $this->assertNotNull($edge2 = $this->getPathCount()->getEdge('A', 'B'));
        $this->assertSame('A', $edge2->start_vertex);
        $this->assertSame('B', $edge2->end_vertex);
    }

    public function test_can_get_a_transitive_closure_edge()
    {
        $this->getPathCount()->createEdge('A', 'B');
        $this->getPathCount()->createEdge('B', 'C');

        $this->assertNull($this->getPathCount()->getGraphEdge('A', 'C'));
        $this->assertNotNull($edge = $this->getPathCount()->getEdge('A', 'C'));
        $this->assertSame('A', $edge->start_vertex);
        $this->assertSame('C', $edge->end_vertex);
    }

    public function test_can_rebuild_mutated_diagonal_diamond()
    {
        $this->createDiagonalDiamond();

        // Add erroneous TC edges
        foreach ([['C', 'D'], ['E', 'G'], ['G', 'H']] as [$start, $end]) {
            DB::table('situation_path_count_algorithm_string_edge')->insert([
                'start_vertex' => $start,
                'end_vertex' => $end,
                'path_count' => 1,
                'edge_type' => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        // Remove existing TC edge
        DB::table('situation_path_count_algorithm_string_edge')
            ->where('start_vertex', '=', 'A')
            ->where('end_vertex', '=', 'E')
            ->delete();

        $this->assertEdges(
            [
                ['A', 'B', 'path_count' => 1],
                ['A', 'C', 'path_count' => 1],
                ['B', 'D', 'path_count' => 1],
                ['B', 'E', 'path_count' => 1],
                ['C', 'E', 'path_count' => 1],
                ['D', 'F', 'path_count' => 1],
                ['E', 'F', 'path_count' => 1],
            ],
            [
                ['A', 'D', 'path_count' => 1],
                ['B', 'F', 'path_count' => 2],
                //['A', 'E', 'path_count' => 2],
                ['C', 'F', 'path_count' => 1],
                ['A', 'F', 'path_count' => 3],
                ['C', 'D', 'path_count' => 1], // !
                ['E', 'G', 'path_count' => 1], // !
                ['G', 'H', 'path_count' => 1], // !
            ],
            [],
            $this->getAllEdges()
        );

        $this->getPathCount()->rebuild();

        $this->assertEdges(
            [
                ['A', 'B', 'path_count' => 1],
                ['A', 'C', 'path_count' => 1],
                ['B', 'D', 'path_count' => 1],
                ['B', 'E', 'path_count' => 1],
                ['C', 'E', 'path_count' => 1],
                ['D', 'F', 'path_count' => 1],
                ['E', 'F', 'path_count' => 1],
            ],
            [
                ['A', 'D', 'path_count' => 1],
                ['B', 'F', 'path_count' => 2],
                ['A', 'E', 'path_count' => 2],
                ['C', 'F', 'path_count' => 1],
                ['A', 'F', 'path_count' => 3],
            ],
            [],
            $this->getAllEdges()
        );
    }
}
