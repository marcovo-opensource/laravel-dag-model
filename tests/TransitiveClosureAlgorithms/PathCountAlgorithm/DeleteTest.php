<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithm;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\EdgeNotFoundException;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationPathCountAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;
use PHPUnit\Framework\Attributes\DataProvider;

class DeleteTest extends TestCase
{
    protected function getAlgorithm(): PathCountAlgorithm
    {
        return new PathCountAlgorithm(new SituationPathCountAlgorithmEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_path_count_algorithm_edge')->get();
    }

    public function test_can_delete_an_edge()
    {
        $this->withVertices($V = [1, 2]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2],
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(1, 2);

        $this->assertEdges([], [], $V, $this->getAllEdges());
    }

    public function test_can_delete_edge_from_chain_of_two_edges()
    {
        $this->withVertices($V = [1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2],
                [2, 3],
            ],
            [
                [1, 3],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(1, 2);

        $this->assertEdges(
            [
                [2, 3],
            ],
            [],
            $V,
            $this->getAllEdges()
        );
    }

    public function test_cannot_delete_non_existing_edge()
    {
        $this->withVertices([1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->expectException(EdgeNotFoundException::class);
        $this->getAlgorithm()->deleteEdge(1, 3);
    }

    /**
     * Tests deleting edge (1, 2) from a chain of three edges:
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_delete_first_edge_in_chain_of_three_edges()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(1, 2);

        $this->assertEdges(
            [
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [2, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (2, 3) from a chain of three edges:
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_delete_second_edge_in_chain_of_three_edges()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (3, 4) from a chain of three edges:
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_delete_last_edge_in_chain_of_three_edges()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (3, 4) from a diamond
     *       1
     *      / \
     *     2   3
     *      \ /
     *       4
     */
    public function test_can_delete_bottom_edge_in_diamond()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (3, 4) from a diamond
     *       1
     *      / \
     *     2   3
     *      \ /
     *       4
     */
    public function test_can_delete_top_edge_in_diamond()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (5, 6) from a box diamond
     *       1
     *      / \
     *     2   3
     *     |   |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_delete_bottom_edge_in_box_diamond()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 5, 'path_count' => 1],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 5, 'path_count' => 1],
                [1, 6, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (1, 3) from a box diamond
     *       1
     *      / \
     *     2   3
     *     |   |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_delete_top_edge_in_box_diamond()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 5, 'path_count' => 1],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (3, 5) from a box diamond
     *       1
     *      / \
     *     2   3
     *     |   |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_delete_middle_edge_in_box_diamond()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 5, 'path_count' => 1],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(3, 5);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (4, 5) from a tri-diamond
     *       1
     *      /|\
     *     2 3 4
     *      \|/
     *       5
     */
    public function test_can_delete_bottom_edge_in_tridiamond()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 5);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(1, 4);
        $this->getAlgorithm()->createEdge(4, 5);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
                [4, 5, 'path_count' => 1],
            ],
            [
                [1, 5, 'path_count' => 3],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(4, 5);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            [
                [1, 5, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (1, 4) from a tri-diamond
     *       1
     *      /|\
     *     2 3 4
     *      \|/
     *       5
     */
    public function test_can_delete_top_edge_in_tridiamond()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 5);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(1, 4);
        $this->getAlgorithm()->createEdge(4, 5);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
                [4, 5, 'path_count' => 1],
            ],
            [
                [1, 5, 'path_count' => 3],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(1, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 5, 'path_count' => 1],
            ],
            [
                [1, 5, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    public static function provider_can_delete_edge_from_diagonal_diamond()
    {
        return [
            [
                [1, 2],
                [
                    [2, 6, 'path_count' => 2],
                    [1, 5, 'path_count' => 1],
                    [3, 6, 'path_count' => 1],
                    [1, 6, 'path_count' => 1],
                ]
            ],
            [
                [1, 3],
                [
                    [1, 4, 'path_count' => 1],
                    [2, 6, 'path_count' => 2],
                    [1, 5, 'path_count' => 1],
                    [3, 6, 'path_count' => 1],
                    [1, 6, 'path_count' => 2],
                ]
            ],
            [
                [2, 4],
                [
                    [2, 6, 'path_count' => 1],
                    [1, 5, 'path_count' => 2],
                    [3, 6, 'path_count' => 1],
                    [1, 6, 'path_count' => 2],
                ]
            ],
            [
                [2, 5],
                [
                    [1, 4, 'path_count' => 1],
                    [2, 6, 'path_count' => 1],
                    [1, 5, 'path_count' => 1],
                    [3, 6, 'path_count' => 1],
                    [1, 6, 'path_count' => 2],
                ]
            ],
            [
                [3, 5],
                [
                    [1, 4, 'path_count' => 1],
                    [2, 6, 'path_count' => 2],
                    [1, 5, 'path_count' => 1],
                    [1, 6, 'path_count' => 2],
                ]
            ],
            [
                [4, 6],
                [
                    [1, 4, 'path_count' => 1],
                    [2, 6, 'path_count' => 1],
                    [1, 5, 'path_count' => 2],
                    [3, 6, 'path_count' => 1],
                    [1, 6, 'path_count' => 2],
                ]
            ],
            [
                [5, 6],
                [
                    [1, 4, 'path_count' => 1],
                    [2, 6, 'path_count' => 1],
                    [1, 5, 'path_count' => 2],
                    [1, 6, 'path_count' => 1],
                ]
            ],
        ];
    }

    /**
     * Tests deleting each of the edges in a diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     * @dataProvider provider_can_delete_edge_from_diagonal_diamond
     */
    #[DataProvider('provider_can_delete_edge_from_diagonal_diamond')]
    public function test_can_delete_edge_from_diagonal_diamond(array $deleteEdge, array $remainingTcEdges)
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(2, 5);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(5, 6);

        $graphEdges = [
            [1, 2, 'path_count' => 1],
            [1, 3, 'path_count' => 1],
            [2, 4, 'path_count' => 1],
            [2, 5, 'path_count' => 1],
            [3, 5, 'path_count' => 1],
            [4, 6, 'path_count' => 1],
            [5, 6, 'path_count' => 1],
        ];

        $this->assertEdges(
            $graphEdges,
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 2],
                [1, 5, 'path_count' => 2],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 3],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge($deleteEdge[0], $deleteEdge[1]);
        $graphEdges = array_filter($graphEdges, fn (array $e) => $e[0] !== $deleteEdge[0] || $e[1] !== $deleteEdge[1]);

        $this->assertEdges(
            $graphEdges,
            $remainingTcEdges,
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests deleting edge (2, 4) that should remain in the transitive closure
     *         1
     *        /
     *       2
     *      / \
     *     3 - 4
     */
    public function test_can_delete_edge_that_remains_in_transitive_closure()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(3, 4);
        $this->getAlgorithm()->createEdge(2, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
                [2, 4, 'path_count' => 2],
            ],
            [
                [1, 3, 'path_count' => 1],
                [1, 4, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->deleteEdge(2, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests that updated_at is updated for edges that are removed from the graph but remain in the transitive closure
     *       1
     *       |\
     *       | 2
     *       |/
     *       3
     */
    public function test_updates_updated_at()
    {
        $this->withVertices($V = [1, 2, 3]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        DB::table('situation_path_count_algorithm_edge')->update(['updated_at' => null]);

        $this->getAlgorithm()->deleteEdge(1, 3);

        $this->assertNotNull(
            $this->getAllEdges()
                ->where('start_vertex', '=', 1)
                ->where('end_vertex', '=', 3)
                ->first()
                ->updated_at
        );
    }

    /**
     * Tests that deleting a graph edge does not delete a graph edge that happens to be a potential transitive closure
     * edge
     *       1
     *       |\
     *       | 2
     *       |/
     *       3
     */
    public function test_does_not_incorrectly_delete_graph_edge()
    {
        $this->withVertices($V = [1, 2, 3]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(1, 3);

        $this->getAlgorithm()->deleteEdge(1, 2);

        $this->assertEdges(
            [
                [1, 3, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
            ],
            [],
            $V,
            $this->getAllEdges()
        );
    }
}
