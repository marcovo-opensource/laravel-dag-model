<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithm;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\EdgeNotFoundException;
use Marcovo\LaravelDagModel\Exceptions\PotentialPathCountOverflowException;
use Marcovo\LaravelDagModel\Exceptions\SeparationException;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationPathCountAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;

class SeparationTest extends TestCase
{
    private function getPathCount(\Closure $separationCallback): PathCountAlgorithm
    {
        $pathCount = new PathCountAlgorithm(new SituationPathCountAlgorithmEdge());
        $pathCount->setSeparationCallback($separationCallback);
        return $pathCount;
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_path_count_algorithm_edge')->get();
    }

    /**
     * Tests whether we can build two separated DAGs
     *       1        6
     *      / \      / \
     *     2   3    7   8
     *      \ /      \ /
     *       4        9
     */
    public function test_can_build_separated_dags()
    {
        $pc1 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $pc2 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '>', 5);
        });

        $pc1->createEdge(1, 2);
        $pc1->createEdge(2, 4);
        $pc1->createEdge(1, 3);
        $pc1->createEdge(3, 4);

        $pc2->createEdge(6, 7);
        $pc2->createEdge(7, 9);
        $pc2->createEdge(6, 8);
        $pc2->createEdge(8, 9);

        $edges = $this->getAllEdges()->groupBy(fn ($edge) => ($edge->start_vertex < 5) ? 1 : 2);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 2],
            ],
            [],
            $edges[1]
        );

        $this->assertEdges(
            [
                [6, 7, 'path_count' => 1],
                [7, 9, 'path_count' => 1],
                [6, 8, 'path_count' => 1],
                [8, 9, 'path_count' => 1],
            ],
            [
                [6, 9, 'path_count' => 2],
            ],
            [],
            $edges[2]
        );
    }

    /**
     * Tests whether we can remove edges (3, 4) and (6, 8)
     *       1        6
     *      / \      / \
     *     2   3    7   8
     *      \ /      \ /
     *       4        9
     */
    public function test_can_remove_edges()
    {
        $pc1 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $pc2 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '>', 5);
        });

        $pc1->createEdge(1, 2);
        $pc1->createEdge(2, 4);
        $pc1->createEdge(1, 3);
        $pc1->createEdge(3, 4);

        $pc2->createEdge(6, 7);
        $pc2->createEdge(7, 9);
        $pc2->createEdge(6, 8);
        $pc2->createEdge(8, 9);

        $edges = $this->getAllEdges()->groupBy(fn ($edge) => ($edge->start_vertex < 5) ? 1 : 2);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 2],
            ],
            [],
            $edges[1]
        );

        $this->assertEdges(
            [
                [6, 7, 'path_count' => 1],
                [7, 9, 'path_count' => 1],
                [6, 8, 'path_count' => 1],
                [8, 9, 'path_count' => 1],
            ],
            [
                [6, 9, 'path_count' => 2],
            ],
            [],
            $edges[2]
        );

        $pc1->deleteEdge(3, 4);

        $edges = $this->getAllEdges()->groupBy(fn ($edge) => ($edge->start_vertex < 5) ? 1 : 2);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
            ],
            [],
            $edges[1]
        );

        $this->assertEdges(
            [
                [6, 7, 'path_count' => 1],
                [7, 9, 'path_count' => 1],
                [6, 8, 'path_count' => 1],
                [8, 9, 'path_count' => 1],
            ],
            [
                [6, 9, 'path_count' => 2],
            ],
            [],
            $edges[2]
        );

        $pc2->deleteEdge(6, 8);

        $edges = $this->getAllEdges()->groupBy(fn ($edge) => ($edge->start_vertex < 5) ? 1 : 2);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
            ],
            [],
            $edges[1]
        );

        $this->assertEdges(
            [
                [6, 7, 'path_count' => 1],
                [7, 9, 'path_count' => 1],
                [8, 9, 'path_count' => 1],
            ],
            [
                [6, 9, 'path_count' => 1],
            ],
            [],
            $edges[2]
        );
    }

    public function test_cannot_add_edge_between_separated_dags()
    {
        $pc = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $this->expectException(SeparationException::class);
        $pc->createEdge(4, 6);
    }

    public function test_cannot_add_edge_in_other_dag()
    {
        $pc = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $this->expectException(SeparationException::class);
        $pc->createEdge(6, 7);
    }

    public function test_cannot_delete_edge_in_other_dag()
    {
        $pc1 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $pc2 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '>', 5);
        });

        $pc2->createEdge(6, 7);

        $this->expectException(EdgeNotFoundException::class);
        $pc1->deleteEdge(6, 7);
    }

    public function test_cannot_get_edge_from_another_dag()
    {
        $pc1 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        });

        $pc2 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '>', 5);
        });

        $pc2->createEdge(6, 7);

        $this->assertNull($pc1->getEdge(6, 7));
        $this->assertNull($pc1->getGraphEdge(6, 7));
        $this->assertFalse($pc1->hasEdge(6, 7));
        $this->assertFalse($pc1->hasGraphEdge(6, 7));
    }

    public function test_no_contribution_to_potential_path_count_overflow_between_separated_graphs()
    {
        $pc1 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '<', 5);
        })->setPotentialPathCountOverFlowDetection(PathCountAlgorithm::OVERFLOW_DETECTION_NAIVE);

        $pc2 = $this->getPathCount(function (string $vertexTableAlias, string $columnName) {
            return fn (Builder $query) => $query->where($vertexTableAlias . '.' . $columnName, '>', 5);
        })->setPotentialPathCountOverFlowDetection(PathCountAlgorithm::OVERFLOW_DETECTION_NAIVE);

        $pc1->createEdge(1, 2);

        $pc2->createEdge(6, 7);
        $pc2->createEdge(7, 8);
        $pc2->createEdge(8, 9);

        DB::table('situation_path_count_algorithm_edge')->update(['path_count' => 2**32]);

        // This should succeed
        $pc1->createEdge(2, 3);

        // This should fail
        $this->expectException(PotentialPathCountOverflowException::class);
        $pc2->createEdge(9, 10);
    }
}
