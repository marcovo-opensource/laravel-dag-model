<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithm;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\CycleException;
use Marcovo\LaravelDagModel\Exceptions\DuplicateEdgeException;
use Marcovo\LaravelDagModel\Exceptions\LogicException;
use Marcovo\LaravelDagModel\Exceptions\PotentialPathCountOverflowException;
use Marcovo\LaravelDagModel\Exceptions\SelfLinkException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationPathCountAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;
use PHPUnit\Framework\Attributes\DataProvider;

class CreateTest extends TestCase
{
    protected function getAlgorithm(): PathCountAlgorithm
    {
        return new PathCountAlgorithm(new SituationPathCountAlgorithmEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_path_count_algorithm_edge')->get();
    }

    public function test_can_create_an_edge()
    {
        $this->withVertices($V = [1, 2]);

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1]
            ],
            [],
            $V,
            $this->getAllEdges()
        );
    }

    public function test_can_create_chain_of_two_edges()
    {
        $this->withVertices($V = [1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);


        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    public function test_cannot_add_edge_from_vertex_to_itself()
    {
        $this->withVertices([1]);

        $this->expectException(SelfLinkException::class);
        $this->getAlgorithm()->createEdge(1, 1);
    }

    public function test_cannot_add_edge_twice()
    {
        $this->withVertices([1, 2]);

        $this->getAlgorithm()->createEdge(1, 2);

        $this->expectException(DuplicateEdgeException::class);
        $this->getAlgorithm()->createEdge(1, 2);
    }

    public function test_does_not_allow_direct_cycles()
    {
        $this->withVertices([1, 2]);

        $this->getAlgorithm()->createEdge(1, 2);

        $this->expectException(CycleException::class);
        $this->getAlgorithm()->createEdge(2, 1);
    }

    public function test_does_not_allow_indirect_cycles()
    {
        $this->withVertices([1, 2, 3]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->expectException(CycleException::class);
        $this->getAlgorithm()->createEdge(3, 1);
    }

    /**
     * Tests creation of chain in order (1, 2), (2, 3), (3, 4):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of chain in order (3, 4), (2, 3), (1, 2):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges_reverse()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [3, 4, 'path_count' => 1]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [2, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of chain in order (2, 3), (1, 2), (3, 4):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges_inside_out()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [2, 3, 'path_count' => 1]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of chain in order (1, 2), (3, 4), (2, 3):
     *      1
     *      |
     *      2
     *      |
     *      3
     *      |
     *      4
     */
    public function test_can_create_chain_of_three_edges_outside_in()
    {
        $this->withVertices($V =[1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1]
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of diamond by adding (3, 4) last
     *       1
     *      / \
     *     2   3
     *      \ /
     *       4
     */
    public function test_can_create_diamond_closing_at_bottom()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of diamond by adding (1, 3) last
     *       1
     *      / \
     *     2   3
     *      \ /
     *       4
     */
    public function test_can_create_diamond_closing_at_top()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of box diamond by adding (5, 6) last
     *       1
     *      / \
     *     2   3
     *     |   |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_create_box_diamond_closing_at_bottom()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 5, 'path_count' => 1],
                [1, 6, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 5, 'path_count' => 1],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of box diamond by adding (1, 3) last
     *       1
     *      / \
     *     2   3
     *     |   |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_create_box_diamond_closing_at_top()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 3);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 5, 'path_count' => 1],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of box diamond by adding (3, 5) last
     *       1
     *      / \
     *     2   3
     *     |   |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_create_box_diamond_closing_in_middle()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 4);
        $this->getAlgorithm()->createEdge(4, 6);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(5, 6);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(3, 5);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 1],
                [1, 5, 'path_count' => 1],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of tri-diamond by adding (4, 5) last
     *       1
     *      /|\
     *     2 3 4
     *      \|/
     *       5
     */
    public function test_can_create_tridiamond_closing_at_bottom()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 5);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(1, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            [
                [1, 5, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(4, 5);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
                [4, 5, 'path_count' => 1],
            ],
            [
                [1, 5, 'path_count' => 3],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of tri-diamond by adding (1, 4) last
     *       1
     *      /|\
     *     2 3 4
     *      \|/
     *       5
     */
    public function test_can_create_tridiamond_closing_at_top()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 5);
        $this->getAlgorithm()->createEdge(1, 3);
        $this->getAlgorithm()->createEdge(3, 5);
        $this->getAlgorithm()->createEdge(4, 5);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 5, 'path_count' => 1],
            ],
            [
                [1, 5, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(1, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
                [4, 5, 'path_count' => 1],
            ],
            [
                [1, 5, 'path_count' => 3],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    public static function provider_can_create_diagonal_diamond()
    {
        return [
            [0], [1], [2], [3], [4], [5], [6],
        ];
    }

    /**
     * Tests creation of diagonal diamond by switching which of the 7 edges to add last
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     * @dataProvider provider_can_create_diagonal_diamond
     */
    #[DataProvider('provider_can_create_diagonal_diamond')]
    public function test_can_create_diagonal_diamond(int $skipIndex)
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $vertices = [
            [1, 2],
            [1, 3],
            [2, 4],
            [2, 5],
            [3, 5],
            [4, 6],
            [5, 6],
        ];

        for ($i = 0; $i < 7; $i++) {
            if ($i !== $skipIndex) {
                $this->getAlgorithm()->createEdge($vertices[$i][0], $vertices[$i][1]);
            }
        }

        $this->getAlgorithm()->createEdge($vertices[$skipIndex][0], $vertices[$skipIndex][1]);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 2],
                [1, 5, 'path_count' => 2],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 3],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests creation of edge (2, 4) after already being in the transitive closure
     *         1
     *        /
     *       2
     *      / \
     *     3 - 4
     */
    public function test_can_create_edge_that_already_was_in_transitive_closure()
    {
        $this->withVertices($V = [1, 2, 3, 4]);

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);
        $this->getAlgorithm()->createEdge(3, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
            ],
            [
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [1, 4, 'path_count' => 1],
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->createEdge(2, 4);

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [2, 3, 'path_count' => 1],
                [3, 4, 'path_count' => 1],
                [2, 4, 'path_count' => 2],
            ],
            [
                [1, 3, 'path_count' => 1],
                [1, 4, 'path_count' => 2],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests whether created_at and updated_at are set, on a diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     */
    public function test_sets_timestamps()
    {
        $this->withVertices([1, 2, 3, 4, 5, 6]);

        $vertices = [
            [1, 2],
            [1, 3],
            [2, 4],
            [2, 5],
            [3, 5],
            [4, 6],
            [5, 6],
        ];

        for ($i = 0; $i < 7; $i++) {
            $this->getAlgorithm()->createEdge($vertices[$i][0], $vertices[$i][1]);
        }

        foreach ($this->getAllEdges() as $edge) {
            $this->assertNotNull($edge->created_at);
            $this->assertNotNull($edge->updated_at);
        }

        DB::table('situation_path_count_algorithm_edge')->update(['updated_at' => null]);

        $this->getAlgorithm()->createEdge(5, 7);
        $this->assertNull(
            $this->getAllEdges()
                ->where('start_vertex', 5)
                ->where('end_vertex', 6)
                ->first()->updated_at
        );

        $this->getAlgorithm()->createEdge(7, 6);
        $this->assertNotNull(
            $this->getAllEdges()
                ->where('start_vertex', 5)
                ->where('end_vertex', 6)
                ->first()->updated_at
        );
    }

    /**
     * Tests that creating 33 chained diamonds (or more) triggers the potential path count overflow detection
     */
    public function test_fails_when_potential_path_count_overflow_is_detected()
    {
        $dbEngine = env('DB_CONNECTION');
        if ($dbEngine === 'postgresql' && static::isGitlabCiServer()) {
            $this->markTestSkipped('GitLab CI server sometimes is terribly slow on this test with PostgreSQL and php >= 8');
        }
        $pathCount = $this->getAlgorithm()->setPotentialPathCountOverFlowDetection(PathCountAlgorithm::OVERFLOW_DETECTION_NAIVE);

        $this->withVertices([1]);

        for ($i = 0; $i < 32; $i++) {
            $this->withVertices([
                $i * 3 + 2,
                $i * 3 + 3,
                $i * 3 + 4,
            ]);

            $pathCount->createEdge($i * 3 + 1, $i * 3 + 2);
            $pathCount->createEdge($i * 3 + 2, $i * 3 + 4);
            $pathCount->createEdge($i * 3 + 1, $i * 3 + 3);
            $pathCount->createEdge($i * 3 + 3, $i * 3 + 4);
        }

        $this->assertSame(32 * 4, DB::table('situation_path_count_algorithm_edge')->where('edge_type', '=', IsEdgeInDagContract::TYPE_GRAPH_EDGE)->count());

        $this->withVertices([
            $i * 3 + 2,
        ]);

        $i = 32;
        $this->expectException(PotentialPathCountOverflowException::class);
        $pathCount->createEdge($i * 3 + 1, $i * 3 + 2);
    }

    /**
     * Tests that creating 64 chained diamonds (or more) fails the algorithm
     */
    public function test_fails_when_path_count_overflows()
    {
        $dbEngine = env('DB_CONNECTION');
        if ($dbEngine === 'sqlite') {
            $this->markTestSkipped('Sqlite does not error on overflow');
        } elseif ($dbEngine === 'postgresql') {
            if (static::isGitlabCiServer()) {
                $this->markTestSkipped('GitLab CI server sometimes is terribly slow on this test with PostgreSQL and php >= 8');
            }
            $max = 62;
        } elseif ($dbEngine === 'mariadb') {
            $max = 63;
        } else {
            throw new \Exception();
        }

        $pathCount = $this->getAlgorithm()->setPotentialPathCountOverFlowDetection(PathCountAlgorithm::OVERFLOW_DETECTION_OFF);

        $this->withVertices([1]);

        for ($i = 0; $i < $max; $i++) {
            $this->withVertices([
                $i * 3 + 2,
                $i * 3 + 3,
                $i * 3 + 4,
            ]);

            $pathCount->createEdge($i * 3 + 1, $i * 3 + 2);
            $pathCount->createEdge($i * 3 + 2, $i * 3 + 4);
            $pathCount->createEdge($i * 3 + 1, $i * 3 + 3);
            $pathCount->createEdge($i * 3 + 3, $i * 3 + 4);
        }

        $this->assertSame($max * 4, DB::table('situation_path_count_algorithm_edge')->where('edge_type', '=', IsEdgeInDagContract::TYPE_GRAPH_EDGE)->count());

        $this->withVertices([
            $i * 3 + 2,
            $i * 3 + 3,
            $i * 3 + 4,
        ]);

        $i = $max;
        $pathCount->createEdge($i * 3 + 1, $i * 3 + 2);
        $pathCount->createEdge($i * 3 + 2, $i * 3 + 4);
        $pathCount->createEdge($i * 3 + 1, $i * 3 + 3);

        $this->expectException(\PDOException::class);
        $pathCount->createEdge($i * 3 + 3, $i * 3 + 4);
    }

    public function test_fails_on_invalid_overflow_setting()
    {
        $pathCount = $this->getAlgorithm();

        $this->expectException(LogicException::class);
        $pathCount->setPotentialPathCountOverFlowDetection('foo');
    }
}
