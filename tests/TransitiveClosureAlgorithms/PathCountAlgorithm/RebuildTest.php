<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithm;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationPathCountAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;

class RebuildTest extends TestCase
{
    protected function getAlgorithm(): PathCountAlgorithm
    {
        return new PathCountAlgorithm(new SituationPathCountAlgorithmEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_path_count_algorithm_edge')->get();
    }

    /**
     * Tests rebuilding a mutated diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_rebuild_mutated_diagonal_diamond()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $vertices = [
            [1, 2],
            [1, 3],
            [2, 4],
            [2, 5],
            [3, 5],
            [4, 6],
            [5, 6],
        ];

        for ($i = 0; $i < 7; $i++) {
            $this->getAlgorithm()->createEdge($vertices[$i][0], $vertices[$i][1]);
        }

        // Add erroneous TC edges
        foreach ([[3, 4], [5, 7], [7, 8]] as [$start, $end]) {
            DB::table('situation_path_count_algorithm_edge')->insert([
                'start_vertex' => $start,
                'end_vertex' => $end,
                'path_count' => 1,
                'edge_type' => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        // Remove existing TC edge
        DB::table('situation_path_count_algorithm_edge')
            ->where('start_vertex', '=', 1)
            ->where('end_vertex', '=', 5)
            ->delete();

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 2],
                //[1, 5, 'path_count' => 2],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 3],
                [3, 4, 'path_count' => 1], // !
                [5, 7, 'path_count' => 1], // !
                [7, 8, 'path_count' => 1], // !
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->rebuild();

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 2],
                [1, 5, 'path_count' => 2],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 3],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests rebuilding a stripped diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_rebuild_stripped_diagonal_diamond()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $vertices = [
            [1, 2],
            [1, 3],
            [2, 4],
            [2, 5],
            [3, 5],
            [4, 6],
            [5, 6],
        ];

        for ($i = 0; $i < 7; $i++) {
            $this->getAlgorithm()->createEdge($vertices[$i][0], $vertices[$i][1]);
        }

        // Remove existing TC edges
        DB::table('situation_path_count_algorithm_edge')
            ->where('edge_type', '=', IsEdgeInDagContract::TYPE_CLOSURE_EDGE)
            ->delete();

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->rebuild();

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 2],
                [1, 5, 'path_count' => 2],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 3],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests rebuilding a stuffed diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     */
    public function test_can_rebuild_stuffed_diagonal_diamond()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $vertices = [
            [1, 2],
            [1, 3],
            [2, 4],
            [2, 5],
            [3, 5],
            [4, 6],
            [5, 6],
        ];

        for ($i = 0; $i < 7; $i++) {
            $this->getAlgorithm()->createEdge($vertices[$i][0], $vertices[$i][1]);
        }

        // Add erroneous TC edges
        foreach ([[3, 4], [5, 7], [7, 8], [8, 6], [3, 2], [6, 4]] as [$start, $end]) {
            DB::table('situation_path_count_algorithm_edge')->insert([
                'start_vertex' => $start,
                'end_vertex' => $end,
                'path_count' => 1,
                'edge_type' => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 2],
                [1, 5, 'path_count' => 2],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 3],
                [3, 4, 'path_count' => 1], // !
                [5, 7, 'path_count' => 1], // !
                [7, 8, 'path_count' => 1], // !
                [8, 6, 'path_count' => 1], // !
                [3, 2, 'path_count' => 1], // !
                [6, 4, 'path_count' => 1], // !
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->rebuild();

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 2],
                [1, 5, 'path_count' => 2],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 3],
            ],
            $V,
            $this->getAllEdges()
        );
    }

    /**
     * Tests that an exception is thrown when a future edge is present
     */
    public function test_fails_on_future_edge()
    {
        $this->withVertices($V = [1, 2]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $this->getAlgorithm()->createEdge(1, 2);

        DB::table('situation_path_count_algorithm_edge')->update([
            'updated_at' => Carbon::now()->addSeconds(3)->toDateTimeString(),
        ]);

        $this->expectException(LaravelDagModelException::class);
        $this->expectExceptionMessage('Cannot rebuild a TC with an updated_at in the future');
        $this->getAlgorithm()->rebuild();
    }

    /**
     * Tests rebuilding two diagonal diamonds in separated graphs
     *       1           11
     *      / \         / \
     *     2   3      12   13
     *     | \ |       | \ |
     *     4   5      14   15
     *      \ /         \ /
     *       6           16
     */
    public function test_can_rebuild_separated_diagonal_diamonds()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6, 11, 12, 13, 14, 15, 16]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $vertices = [
            [1, 2],
            [1, 3],
            [2, 4],
            [2, 5],
            [3, 5],
            [4, 6],
            [5, 6],
            [11, 12],
            [11, 13],
            [12, 14],
            [12, 15],
            [13, 15],
            [14, 16],
            [15, 16],
        ];

        for ($i = 0; $i < count($vertices); $i++) {
            $this->getAlgorithm()->createEdge($vertices[$i][0], $vertices[$i][1]);
        }

        // Add erroneous TC edges
        foreach ([[3, 4], [13, 14]] as [$start, $end]) {
            DB::table('situation_path_count_algorithm_edge')->insert([
                'start_vertex' => $start,
                'end_vertex' => $end,
                'path_count' => 1,
                'edge_type' => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        // Remove existing TC edge
        foreach ([[1, 5], [11, 15]] as [$start, $end]) {
            DB::table('situation_path_count_algorithm_edge')
                ->where('start_vertex', '=', $start)
                ->where('end_vertex', '=', $end)
                ->delete();
        }

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
                [11, 12, 'path_count' => 1],
                [11, 13, 'path_count' => 1],
                [12, 14, 'path_count' => 1],
                [12, 15, 'path_count' => 1],
                [13, 15, 'path_count' => 1],
                [14, 16, 'path_count' => 1],
                [15, 16, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 2],
                //[1, 5, 'path_count' => 2],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 3],
                [3, 4, 'path_count' => 1], // !
                [11, 14, 'path_count' => 1],
                [12, 16, 'path_count' => 2],
                //[11, 15, 'path_count' => 2],
                [13, 16, 'path_count' => 1],
                [11, 16, 'path_count' => 3],
                [13, 14, 'path_count' => 1], // !
            ],
            $V,
            $this->getAllEdges()
        );

        $pathCount = $this->getAlgorithm();
        $pathCount->setSeparationCallback(function ($table, $vertex) {
            return function ($query) use ($vertex) {
                $query->where($vertex, '<', 10);
            };
        });
        $pathCount->rebuild();

        $this->assertEdges(
            [
                [1, 2, 'path_count' => 1],
                [1, 3, 'path_count' => 1],
                [2, 4, 'path_count' => 1],
                [2, 5, 'path_count' => 1],
                [3, 5, 'path_count' => 1],
                [4, 6, 'path_count' => 1],
                [5, 6, 'path_count' => 1],
                [11, 12, 'path_count' => 1],
                [11, 13, 'path_count' => 1],
                [12, 14, 'path_count' => 1],
                [12, 15, 'path_count' => 1],
                [13, 15, 'path_count' => 1],
                [14, 16, 'path_count' => 1],
                [15, 16, 'path_count' => 1],
            ],
            [
                [1, 4, 'path_count' => 1],
                [2, 6, 'path_count' => 2],
                [1, 5, 'path_count' => 2],
                [3, 6, 'path_count' => 1],
                [1, 6, 'path_count' => 3],
                [11, 14, 'path_count' => 1],
                [12, 16, 'path_count' => 2],
                //[11, 15, 'path_count' => 2],
                [13, 16, 'path_count' => 1],
                [11, 16, 'path_count' => 3],
                [13, 14, 'path_count' => 1], // !
            ],
            $V,
            $this->getAllEdges()
        );
    }
}
