<?php

namespace Marcovo\LaravelDagModel\Tests\Helpers;

use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Helpers\LinkingHelper;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation1Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class LinkingHelperTest extends TestCase
{
    public function testLinkParentsToDescendantChildrenFailsWhenNotForest()
    {
        $class = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        LinkingHelper::linkParentToDescendantChildren($class);
    }
}
