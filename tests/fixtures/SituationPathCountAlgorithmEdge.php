<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Edge\PathCountAlgorithmEdge;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;

/**
 * @mixin Builder
 */
class SituationPathCountAlgorithmEdge extends PathCountAlgorithmEdge
{
    protected $table = 'situation_path_count_algorithm_edge';

    public function getVertexModel(): IsVertexInDagContract
    {
        return new class extends DagVertexModel {
            protected $keyType = 'int';

            public function getEdgeModel(): IsEdgeInDagContract
            {
                throw new \Exception();
            }
        };
    }
}
