<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * @mixin Builder
 */
class Situation7DlswVertex extends DagVertexModel
{
    protected $table = 'situation_7_vertex';

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new Situation7DlswEdge();
    }
}
