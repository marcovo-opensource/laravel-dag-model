<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * @mixin Builder
 */
class Situation1Vertex extends DagVertexModel
{
    protected $table = 'situation_1_vertex';

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new Situation1Edge();
    }
}
