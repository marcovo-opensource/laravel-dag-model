<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * @mixin Builder
 */
class Situation3Vertex extends DagVertexModel
{
    protected $table = 'situation_3_vertex';

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new Situation3Edge();
    }
}
