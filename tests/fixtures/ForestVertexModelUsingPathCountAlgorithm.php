<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Extensions\IsForest;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;

/**
 * Forest vertex model using path count algorithm
 * @mixin Builder
 */
class ForestVertexModelUsingPathCountAlgorithm extends Situation1Vertex
{
    use IsForest;

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new class extends Situation1Edge
        {
            public function getVertexModel(): IsVertexInDagContract
            {
                return new ForestVertexModelUsingPathCountAlgorithm();
            }
        };
    }
}
