<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * @mixin Builder
 */
class Situation8Vertex extends DagVertexModel
{
    protected $table = 'situation_8_vertex';

    public $primaryKey = 'uuid';

    public $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (self $model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid()->toString();
            }
        });
    }

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new Situation8Edge();
    }
}
