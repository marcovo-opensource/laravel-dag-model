<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\Edge\PathCountAlgorithmEdge;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;

/**
 * @mixin Builder
 */
class Situation6Edge extends PathCountAlgorithmEdge
{
    protected $table = 'situation_6_edge';

    public function getVertexModel(): IsVertexInDagContract
    {
        return new Situation6Vertex();
    }
}
