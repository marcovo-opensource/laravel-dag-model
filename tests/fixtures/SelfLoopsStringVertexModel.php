<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Extensions\WithSelfLoops;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;

/**
 * @mixin Builder
 */
class SelfLoopsStringVertexModel extends Situation8Vertex
{
    use WithSelfLoops;

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new class extends Situation8Edge
        {
            public function getVertexModel(): IsVertexInDagContract
            {
                return new SelfLoopsStringVertexModel();
            }
        };
    }
}
