<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * @mixin Builder
 */
class Situation4Vertex extends DagVertexModel
{
    protected $table = 'situation_4_vertex';

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new Situation4Edge();
    }

    public function getSeparationCondition(): ?\Closure
    {
        return function ($query) {
            $query->where('separation_column', '=', $this->separation_column);
        };
    }
}
