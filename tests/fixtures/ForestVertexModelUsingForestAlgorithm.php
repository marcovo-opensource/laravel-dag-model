<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Extensions\IsForest;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;

/**
 * @mixin Builder
 */
class ForestVertexModelUsingForestAlgorithm extends Situation7Vertex
{
    use IsForest;

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new class extends Situation7Edge
        {
            public function getVertexModel(): IsVertexInDagContract
            {
                return new ForestVertexModelUsingForestAlgorithm();
            }
        };
    }
}
