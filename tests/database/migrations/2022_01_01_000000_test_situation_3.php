<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestSituation3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('situation_3_vertex', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dummy_vertex_column', 32)->nullable();
            $table->timestamps();
        });

        Schema::create('situation_3_edge', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('start_vertex');
            $table->unsignedBigInteger('end_vertex');
            $table->unsignedBigInteger('path_count');
            $table->unsignedTinyInteger('edge_type');
            $table->string('dummy_edge_column', 32)->nullable();
            $table->timestamps();
            $table->unique(['start_vertex', 'end_vertex']);
            $table->index(['end_vertex']);

            $table->foreign('start_vertex', 'situation3_start_vertex')->on('situation_3_vertex')->references('id')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('end_vertex', 'situation3_end_vertex')->on('situation_3_vertex')->references('id')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situation_3_edge');

        Schema::dropIfExists('situation_3_vertex');
    }
}
