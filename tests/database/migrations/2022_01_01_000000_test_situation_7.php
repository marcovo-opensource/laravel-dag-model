<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestSituation7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('situation_7_vertex', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });

        Schema::create('situation_7_edge', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('start_vertex');
            $table->unsignedBigInteger('end_vertex');
            $table->unsignedTinyInteger('edge_type');
            $table->timestamps();
            $table->unique(['start_vertex', 'end_vertex']);
            $table->index(['end_vertex']);

            $table->foreign('start_vertex', 'situation7_start_vertex')->on('situation_7_vertex')->references('id')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('end_vertex', 'situation7_end_vertex')->on('situation_7_vertex')->references('id')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situation_7_edge');

        Schema::dropIfExists('situation_7_vertex');
    }
}
