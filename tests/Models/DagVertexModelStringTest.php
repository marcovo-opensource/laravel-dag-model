<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Marcovo\LaravelDagModel\Tests\fixtures\Situation8Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class DagVertexModelStringTest extends TestCase
{
    public function testCanAttachChild()
    {
        $child = Situation8Vertex::create();
        $parent = Situation8Vertex::create();

        $parent->children()->attach($child);

        $children = $parent->children;
        $this->assertCount(1, $children);
        $this->assertTrue($child->is($children->first()));
    }

    public function testCanDetachChild()
    {
        $parent = Situation8Vertex::create();
        $child1 = Situation8Vertex::create();
        $child2 = Situation8Vertex::create();
        $child3 = Situation8Vertex::create();

        $parent->children()->attach($child1);
        $parent->children()->attach($child2);
        $parent->children()->attach($child3);

        $children = $parent->children()->get();
        $this->assertCount(3, $children);

        $parent->children()->detach($child2);

        $children = $parent->children()->get();
        $this->assertCount(2, $children);
        $this->assertTrue($child1->is($children->find($child1->uuid)));
        $this->assertTrue($child3->is($children->find($child3->uuid)));
    }

    public function testCanGetChildren()
    {
        $grandparent = Situation8Vertex::create();
        $parent = Situation8Vertex::create();
        $child = Situation8Vertex::create();
        $grandchild = Situation8Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $children = $parent->children()->get();
        $this->assertCount(1, $children);

        $this->assertTrue($child->is($children[0]));
    }

    public function testCanGetDescendants()
    {
        $grandparent = Situation8Vertex::create();
        $parent = Situation8Vertex::create();
        $child = Situation8Vertex::create();
        $grandchild = Situation8Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $descendants = $parent->descendants()->get();
        $this->assertCount(2, $descendants);

        $this->assertTrue($child->is($descendants->find($child->uuid)));
        $this->assertTrue($grandchild->is($descendants->find($grandchild->uuid)));
    }

    public function testCanGetDescendantsWithSelf()
    {
        $grandparent = Situation8Vertex::create();
        $parent = Situation8Vertex::create();
        $child = Situation8Vertex::create();
        $grandchild = Situation8Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $descendants = $parent->descendantsWithSelf()->get();
        $this->assertCount(3, $descendants);

        $this->assertTrue($child->is($descendants->find($child->uuid)));
        $this->assertTrue($grandchild->is($descendants->find($grandchild->uuid)));
        $this->assertTrue($parent->is($descendants->find($parent->uuid)));
    }
}
