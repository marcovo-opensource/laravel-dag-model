<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation6Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class TopOrderSeparationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Situation6Vertex::unguard();
    }

    public function testOrderingsAreIndependentWhenCreating()
    {
        $v1 = Situation6Vertex::create(['separation_column' => 'foo']);
        $this->assertSame(0, $v1->top_order);

        $v2 = Situation6Vertex::create(['separation_column' => 'foo']);
        $this->assertSame(1, $v2->top_order);

        $u1 = Situation6Vertex::create(['separation_column' => 'bar']);
        $this->assertSame(0, $u1->top_order);
    }

    public function testOrderingsAreIndependentWhenAttaching()
    {
        $fetchTopOrder = fn () => DB::table('situation_6_vertex')
            ->orderBy('id')
            ->pluck('top_order', 'id')
            ->map(fn ($x) => intval($x))
            ->values()
            ->all();

        $v1 = Situation6Vertex::create(['separation_column' => 'foo']);
        $v2 = Situation6Vertex::create(['separation_column' => 'foo']);
        $v3 = Situation6Vertex::create(['separation_column' => 'foo']);
        $v4 = Situation6Vertex::create(['separation_column' => 'foo']);

        $u1 = Situation6Vertex::create(['separation_column' => 'bar']);
        $u2 = Situation6Vertex::create(['separation_column' => 'bar']);
        $u3 = Situation6Vertex::create(['separation_column' => 'bar']);
        $u4 = Situation6Vertex::create(['separation_column' => 'bar']);

        $this->assertSame([
            0,
            1,
            2,
            3,
            0,
            1,
            2,
            3,
        ], $fetchTopOrder());

        $v2->parents()->attach($v4);

        $this->assertSame([
            0,
            2,
            3,
            1,
            0,
            1,
            2,
            3,
        ], $fetchTopOrder());

        $u1->parents()->attach($u3);

        $this->assertSame([
            0,
            2,
            3,
            1,
            1,
            2,
            0,
            3,
        ], $fetchTopOrder());
    }

    public function testOrderingsAreIndependentWhenDeleting()
    {
        $fetchTopOrder = fn () => DB::table('situation_6_vertex')
            ->orderBy('id')
            ->pluck('top_order', 'id')
            ->map(fn ($x) => intval($x))
            ->values()
            ->all();

        $v1 = Situation6Vertex::create(['separation_column' => 'foo']);
        $v2 = Situation6Vertex::create(['separation_column' => 'foo']);
        $v3 = Situation6Vertex::create(['separation_column' => 'foo']);
        $v4 = Situation6Vertex::create(['separation_column' => 'foo']);

        $u1 = Situation6Vertex::create(['separation_column' => 'bar']);
        $u2 = Situation6Vertex::create(['separation_column' => 'bar']);
        $u3 = Situation6Vertex::create(['separation_column' => 'bar']);
        $u4 = Situation6Vertex::create(['separation_column' => 'bar']);

        $this->assertSame([
            0,
            1,
            2,
            3,
            0,
            1,
            2,
            3,
        ], $fetchTopOrder());

        $v2->delete();

        $this->assertSame([
            0,
            1,
            2,
            0,
            1,
            2,
            3,
        ], $fetchTopOrder());

        $u3->delete();

        $this->assertSame([
            0,
            1,
            2,
            0,
            1,
            2,
        ], $fetchTopOrder());
    }

    public function testOrderingsAreIndependentWhenRebuilding()
    {
        $v4 = Situation6Vertex::create(['separation_column' => 'foo']);
        $v3 = Situation6Vertex::create(['separation_column' => 'foo']);
        $v2 = Situation6Vertex::create(['separation_column' => 'foo']);
        $v1 = Situation6Vertex::create(['separation_column' => 'foo']);

        $u4 = Situation6Vertex::create(['separation_column' => 'bar']);
        $u3 = Situation6Vertex::create(['separation_column' => 'bar']);
        $u2 = Situation6Vertex::create(['separation_column' => 'bar']);
        $u1 = Situation6Vertex::create(['separation_column' => 'bar']);

        $v1->children()->attach($v2);
        $v2->children()->attach($v3);
        $v3->children()->attach($v4);

        $u1->children()->attach($u2);
        $u2->children()->attach($u3);
        $u3->children()->attach($u4);

        DB::table('situation_6_vertex')->update(['updated_at' => null]);

        $v1->rebuildTopologicalOrdering();

        $this->assertNotNull($v1->refresh()->updated_at);
        $this->assertNotNull($v2->refresh()->updated_at);
        $this->assertNotNull($v3->refresh()->updated_at);
        $this->assertNotNull($v4->refresh()->updated_at);
        $this->assertNull($u1->refresh()->updated_at);
        $this->assertNull($u2->refresh()->updated_at);
        $this->assertNull($u3->refresh()->updated_at);
        $this->assertNull($u4->refresh()->updated_at);

        $this->assertGreaterThan($v1->top_order, $v2->top_order);
        $this->assertGreaterThan($v2->top_order, $v3->top_order);
        $this->assertGreaterThan($v3->top_order, $v4->top_order);
    }
}
