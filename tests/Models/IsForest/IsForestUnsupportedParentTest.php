<?php

namespace Marcovo\LaravelDagModel\Tests\Models\IsForest;

use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Tests\fixtures\ForestVertexModelUsingPathCountAlgorithm;
use Marcovo\LaravelDagModel\Tests\TestCase;

class IsForestUnsupportedParentTest extends TestCase
{
    public function testCannotPerformParentFirstOrNew()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->firstOrNew([]);
    }

    public function testCannotCallFirstOrCreate()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->firstOrCreate([]);
    }

    public function testCannotCallUpdateOrCreate()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->updateOrCreate([]);
    }

    public function testCannotCallCreate()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->create();
    }

    public function testCannotCallInsert()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->insert([]);
    }

    public function testCannotCallInsertOrIgnore()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->insertOrIgnore([]);
    }

    public function testCannotCallInsertGetId()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->insertGetId([]);
    }

    public function testCannotCallInsertUsing()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->insertUsing([], '');
    }

    public function testCannotCallUpdateOrInsert()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->updateOrInsert([]);
    }

    public function testCannotCallDelete()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->delete();
    }

    public function testCannotCallForceDelete()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->parent()->forceDelete();
    }
}
