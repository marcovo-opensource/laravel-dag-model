<?php

namespace Marcovo\LaravelDagModel\Tests\Models\IsForest;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\InDegreeException;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Tests\fixtures\ForestVertexModelUsingForestAlgorithm;
use Marcovo\LaravelDagModel\Tests\fixtures\ForestVertexModelUsingPathCountAlgorithm;
use Marcovo\LaravelDagModel\Tests\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;

abstract class AbstractIsForestTestBase extends TestCase
{
    /**
     * @return ForestVertexModelUsingPathCountAlgorithm|ForestVertexModelUsingForestAlgorithm
     */
    abstract public static function getForestVertexModel(): DagVertexModel;

    public function testSiblingsHasCorrectRelationName()
    {
        $this->assertSame('siblings', static::getForestVertexModel()::make()->siblings()->getRelationName());
    }

    public function testSiblingsAndSelfHasCorrectRelationName()
    {
        $this->assertSame('siblingsAndSelf', static::getForestVertexModel()::make()->siblingsAndSelf()->getRelationName());
    }

    public function testMaxInDegree()
    {
        $child = static::getForestVertexModel()::create();
        $parent1 = static::getForestVertexModel()::create();
        $parent2 = static::getForestVertexModel()::create();

        $child->parents()->attach($parent1);
        $this->expectException(InDegreeException::class);
        $child->parents()->attach($parent2);
    }

    /**
     *              GP -----------
     *              |             \
     *           -- P --          OP
     *          / /  \  \         |
     *         C1 C2 C3 C4        OC
     *            /   \           |
     *          GC21 GC31        OGC
     *
     * @return array|self::static::getForestVertexModel()[]
     */
    private function buildTree(): array
    {
        $parent = static::getForestVertexModel()::create();
        $child1 = static::getForestVertexModel()::create();
        $child2 = static::getForestVertexModel()::create();
        $child3 = static::getForestVertexModel()::create();
        $child4 = static::getForestVertexModel()::create();
        $grandchild21 = static::getForestVertexModel()::create();
        $grandchild31 = static::getForestVertexModel()::create();
        $grandparent = static::getForestVertexModel()::create();

        $parent->children()->attach($child1);
        $parent->children()->attach($child2);
        $parent->children()->attach($child3);
        $parent->children()->attach($child4);

        $child2->children()->attach($grandchild21);
        $child3->children()->attach($grandchild31);

        $grandparent->children()->attach($parent);

        $otherParent = static::getForestVertexModel()::create();
        $otherChild = static::getForestVertexModel()::create();
        $otherGrandchild = static::getForestVertexModel()::create();
        $grandparent->children()->attach($otherParent);
        $otherParent->children()->attach($otherChild);
        $otherChild->children()->attach($otherGrandchild);

        return [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent];
    }

    public function testCanGetParent()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $this->assertTrue($grandparent->is($parent->parent));
        $this->assertTrue($parent->is($child1->parent));
        $this->assertTrue($parent->is($child2->parent));
        $this->assertTrue($parent->is($child3->parent));
        $this->assertTrue($parent->is($child4->parent));
        $this->assertTrue($child2->is($grandchild21->parent));
        $this->assertTrue($child3->is($grandchild31->parent));
    }

    public function testCanCountParent()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $this->assertSame(0, $grandparent->parent()->count());
        $this->assertSame(1, $parent->parent()->count());
    }

    public function testCanGetWhereHasParent()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $vertices = static::getForestVertexModel()::query()
            ->whereHas('parent', function (Builder $query) use ($parent) {
                $query->whereKey($parent->id);
            })
            ->orderBy('id')
            ->pluck('id')
            ->all();

        $this->assertSame([
            $child1->id,
            $child2->id,
            $child3->id,
            $child4->id,
        ], $vertices);
    }

    public function testSiblings()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $query = $child2->siblings();
        $siblings = $query->orderBy($query->qualifyColumn('id'))->pluck($query->qualifyColumn('id'));

        $this->assertSame([
            $child1->id,
            $child3->id,
            $child4->id,
        ], $siblings->map(fn($x) => intval($x))->all());
    }

    public function testSiblingsAndSelf()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $query = $child2->siblingsAndSelf();
        $siblings = $query->orderBy($query->qualifyColumn('id'))->pluck($query->qualifyColumn('id'));

        $this->assertSame([
            $child1->id,
            $child2->id,
            $child3->id,
            $child4->id,
        ], $siblings->map(fn($x) => intval($x))->all());

        $this->assertCount(0, $grandparent->siblingsAndSelf); // 0 instead of 1 because $grandparent has no parent
    }

    public function testSiblingsCount()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $this->assertSame(0, (int)$grandparent->loadCount('siblings')->siblings_count);
        $this->assertSame(0, (int)$grandparent->loadCount('siblingsAndSelf')->siblings_and_self_count);

        $this->assertSame(3, (int)$child2->loadCount('siblings')->siblings_count);
        $this->assertSame(4, (int)$child2->loadCount('siblingsAndSelf')->siblings_and_self_count);

        $this->assertSame(0, (int)$grandchild31->loadCount('siblings')->siblings_count);
        $this->assertSame(1, (int)$grandchild31->loadCount('siblingsAndSelf')->siblings_and_self_count);
    }

    public function testEagerLoadSiblings()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $child2b = static::getForestVertexModel()::with('siblings')->find($child2->id);

        $this->assertSame([
            $child1->id,
            $child3->id,
            $child4->id,
        ], $child2b->siblings->sortBy('id')->map(fn($x) => intval($x->id))->values()->all());
    }

    public function testEagerLoadSiblingsAndSelf()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $child2b = static::getForestVertexModel()::with('siblingsAndSelf')->find($child2->id);

        $this->assertSame([
            $child1->id,
            $child2->id,
            $child3->id,
            $child4->id,
        ], $child2b->siblingsAndSelf->sortBy('id')->map(fn($x) => intval($x->id))->values()->all());
    }

    public function testWhereHasSiblings()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $query = static::getForestVertexModel()::query();
        $siblings = $query->whereHas('siblings', function (Builder $query) use ($child2) {
            $query->where($query->qualifyColumn('id'), '=', $child2->id);
        })->orderBy($query->qualifyColumn('id'))->get();

        $this->assertSame([
            $child1->id,
            $child3->id,
            $child4->id,
        ], $siblings->map(fn($x) => intval($x->id))->all());
    }

    public function testWhereHasSiblingsAndSelf()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $query = static::getForestVertexModel()::query();
        $siblings = $query->whereHas('siblingsAndSelf', function (Builder $query) use ($child2) {
            $query->where($query->qualifyColumn('id'), '=', $child2->id);
        })->orderBy($query->qualifyColumn('id'))->get();

        $this->assertSame([
            $child1->id,
            $child2->id,
            $child3->id,
            $child4->id,
        ], $siblings->map(fn($x) => intval($x->id))->all());
    }

    public function testCanCountSiblings()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $child2b = static::getForestVertexModel()::query()->withCount('siblings')->find($child2->id);

        $this->assertSame(3, (int)$child2b->siblings_count);
    }

    public function testCanCountSiblingsAndSelf()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $child2b = static::getForestVertexModel()::query()->withCount('siblingsAndSelf')->find($child2->id);

        $this->assertSame(4, (int)$child2b->siblings_and_self_count);
    }

    public function testCannotTouchSiblings()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $this->expectException(MethodNotSupportedException::class);
        $child2->siblings()->touch();
    }

    public function testCanFindSiblings()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $this->assertTrue($child1->is($child2->siblings()->find($child1->id)));

        $this->assertSame([
            $child1->id,
            $child3->id,
        ], $child2->siblings()->findMany([$child1->id, $child2->id, $child3->id])->sortBy('id')->pluck('id')->values()->all());
    }

    public function testCanFindSiblingsAndSelf()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $this->assertSame([
            $child1->id,
            $child2->id,
            $child3->id,
        ], $child2->siblingsAndSelf()->findMany([$child1->id, $child2->id, $child3->id])->sortBy('id')->pluck('id')->values()->all());
    }

    public static function providerCanLinkDescendantParent(): array
    {
        return [
            [false],
            [true]
        ];
    }

    /**
     * @dataProvider providerCanLinkDescendantParent
     * @return void
     */
    #[DataProvider('providerCanLinkDescendantParent')]
    public function testCanLinkDescendantParent(bool $descendants)
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $queries = 0;
        DB::listen(function () use (&$queries) { $queries++; });
        if ($descendants) {
            $parent->linkDescendantDescendantsAndParent();
        } else {
            $parent->linkDescendantChildrenAndParent();
        }
        $this->assertSame(2, $queries);

        $child1 = $parent->descendants->find($child1);
        $child2 = $parent->descendants->find($child2);
        $child3 = $parent->descendants->find($child3);
        $child4 = $parent->descendants->find($child4);
        $grandchild21 = $parent->descendants->find($grandchild21);
        $grandchild31 = $parent->descendants->find($grandchild31);

        $this->assertSame($parent, $child1->parent);
        $this->assertSame($parent, $child2->parent);
        $this->assertSame($parent, $child3->parent);
        $this->assertSame($parent, $child4->parent);
        $this->assertSame($child2, $grandchild21->parent);
        $this->assertSame($child3, $grandchild31->parent);

        $this->assertSame(2, $queries);
    }

    public function testCanLinkAncestorParent()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $queries = 0;
        DB::listen(function () use (&$queries) { $queries++; });
        $grandchild21->linkAncestorParent();
        $this->assertSame(2, $queries);

        $grandparent = $grandchild21->ancestors->find($grandparent);
        $parent = $grandchild21->ancestors->find($parent);
        $child2 = $grandchild21->ancestors->find($child2);

        $this->assertSame($child2, $grandchild21->parent);
        $this->assertSame($parent, $child2->parent);
        $this->assertSame($grandparent, $parent->parent);

        $this->assertSame(2, $queries);
    }

    public function testIterateAncestorsInTree()
    {
        [$parent, $child1, $child2, $child3, $child4, $grandchild21, $grandchild31, $grandparent] = $this->buildTree();

        $queries = 0;
        DB::listen(function () use (&$queries) { $queries++; });

        $vertices = [];
        foreach ($grandchild21->iterateAncestorsInTree() as $vertex) {
            $vertices[] = (int)$vertex->id;
        }

        $this->assertSame(2, $queries);

        $this->assertSame([
            $child2->id,
            $parent->id,
            $grandparent->id,
        ], $vertices);
    }

    public function testCanIterateNoAncestorsInTree()
    {
        $vertex = static::getForestVertexModel()::create();

        $count = 0;
        foreach ($vertex->iterateAncestorsInTree() as $vertex) {
            $count++;
        }

        $this->assertSame(0, $count);
    }

    public function testCanAssociateParentModel()
    {
        $parent = static::getForestVertexModel()::create();
        $child = static::getForestVertexModel()::create();

        $result = $child->parent()->associate($parent);

        $this->assertSame($child, $result);

        $this->assertTrue($child->parent->is($parent));
        $this->assertTrue($parent->children->first()->is($child));
    }

    public function testCanAssociateParentId()
    {
        $parent = static::getForestVertexModel()::create();
        $child = static::getForestVertexModel()::create();

        $child->parent()->associate($parent->id);

        $this->assertTrue($child->parent->is($parent));
        $this->assertTrue($parent->children->first()->is($child));
    }

    public function testCanReAssociateTheSameParentModel()
    {
        $parent = static::getForestVertexModel()::create();
        $child = static::getForestVertexModel()::create();

        $child->parent()->associate($parent);

        $queries = 0;
        DB::listen(function () use (&$queries) {$queries++;});
        $child->parent()->associate($parent);
        $this->assertSame(1, $queries);

        $this->assertTrue($child->parent->is($parent));
        $this->assertTrue($parent->children->first()->is($child));
    }

    public function testCanReAssociateTheSameParentId()
    {
        $parent = static::getForestVertexModel()::create();
        $child = static::getForestVertexModel()::create();

        $child->parent()->associate($parent->id);

        $queries = 0;
        DB::listen(function () use (&$queries) {$queries++;});
        $child->parent()->associate($parent->id);
        $this->assertSame(1, $queries);

        $this->assertTrue($child->parent->is($parent));
        $this->assertTrue($parent->children->first()->is($child));
    }

    public function testCanReAssociateAnotherParentModel()
    {
        $parent1 = static::getForestVertexModel()::create();
        $parent2 = static::getForestVertexModel()::create();
        $child = static::getForestVertexModel()::create();

        $child->parent()->associate($parent1);
        $this->assertTrue($child->parent->is($parent1));
        $this->assertTrue($parent1->children->first()->is($child));

        $child->parent()->associate($parent2);
        $this->assertTrue($child->parent->is($parent2));
        $this->assertTrue($parent2->children->first()->is($child));
    }

    public function testCanReAssociateAnotherParentId()
    {
        $parent1 = static::getForestVertexModel()::create();
        $parent2 = static::getForestVertexModel()::create();
        $child = static::getForestVertexModel()::create();

        $child->parent()->associate($parent1->id);
        $this->assertTrue($child->parent->is($parent1));
        $this->assertTrue($parent1->children->first()->is($child));

        $child->parent()->associate($parent2->id);
        $this->assertTrue($child->parent->is($parent2));
        $this->assertTrue($parent2->children->first()->is($child));
    }

    public function testCanDissociateParentModel()
    {
        $parent = static::getForestVertexModel()::create();
        $child = static::getForestVertexModel()::create();

        $child->parent()->associate($parent);

        $this->assertTrue($child->parent->is($parent));
        $result = $child->parent()->dissociate();

        $this->assertSame($child, $result);

        $this->assertNull($child->parent);
        $this->assertNull($child->parent()->first());
    }

    public function testCanDissociateNoParentModel()
    {
        $child = static::getForestVertexModel()::create();

        $child->parent()->dissociate();

        $this->assertNull($child->parent);
    }
}
