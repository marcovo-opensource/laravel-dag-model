<?php

namespace Marcovo\LaravelDagModel\Tests\Models\IsForest;

use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Tests\fixtures\ForestVertexModelUsingForestAlgorithm;

class IsForestUsingForestAlgorithmTest extends AbstractIsForestTestBase
{
    public static function getForestVertexModel(): DagVertexModel
    {
        return new ForestVertexModelUsingForestAlgorithm();
    }
}
