<?php

namespace Marcovo\LaravelDagModel\Tests\Models\IsForest;

use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Tests\fixtures\ForestVertexModelUsingPathCountAlgorithm;
use Marcovo\LaravelDagModel\Tests\TestCase;

class IsForestUnsupportedSiblingsTest extends TestCase
{
    // TODO: Uncomment whenever we drop support for laravel < 9
    // public function testCannotCallFirstOrCreate()
    // {
    //     $vertex = ForestVertexModel::create();
    //
    //     $this->expectException(MethodNotSupportedException::class);
    //     $vertex->siblings()->firstOrCreate([]);
    // }

    public function testCannotCallUpdateOrCreate()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->updateOrCreate([]);
    }

    public function testCannotCallSave()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->save(ForestVertexModelUsingPathCountAlgorithm::make());
    }

    public function testCannotCallSaveMany()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->saveMany([]);
    }

    public function testCannotCallCreate()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->create();
    }

    public function testCannotCallCreateMany()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->createMany([]);
    }

    public function testCannotCallToggle()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->toggle([]);
    }

    public function testCannotCallSync()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->sync([]);
    }

    public function testCannotCallUpdateExistingPivotWithManagedColumns()
    {
        $parent = ForestVertexModelUsingPathCountAlgorithm::create();
        $child = ForestVertexModelUsingPathCountAlgorithm::create();

        $parent->children()->attach($child);

        $this->expectException(MethodNotSupportedException::class);
        $parent->siblings()->updateExistingPivot($child->id, ['start_vertex' => 3]);
    }

    public function testCannotCallUpdateExistingPivotWithUnmanagedColumns()
    {
        $parent = ForestVertexModelUsingPathCountAlgorithm::create();
        $child = ForestVertexModelUsingPathCountAlgorithm::create();

        $parent->children()->attach($child);

        $this->expectException(MethodNotSupportedException::class);
        $parent->siblings()->updateExistingPivot($child, ['created_at' => '2022-01-01 00:00:00']);
    }

    public function testCannotCallAttach()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->attach([]);
    }

    public function testCannotCallDetach()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->detach([]);
    }

    public function testCannotCallInsert()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->insert([]);
    }

    public function testCannotCallInsertOrIgnore()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->insertOrIgnore([]);
    }

    public function testCannotCallInsertGetId()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->insertGetId([]);
    }

    public function testCannotCallInsertUsing()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->insertUsing([], '');
    }

    public function testCannotCallUpdateOrInsert()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->updateOrInsert([]);
    }

    public function testCannotCallDelete()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->delete();
    }

    public function testCannotCallForceDelete()
    {
        $vertex = ForestVertexModelUsingPathCountAlgorithm::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->siblings()->forceDelete();
    }
}
