<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Marcovo\LaravelDagModel\Tests\fixtures\SelfLoopsVertexModel;
use Marcovo\LaravelDagModel\Tests\TestCase;

class BelongsToManyInTransitiveClosureWithoutSelfLoopsTest extends TestCase
{
    public function testNewPivotStatement()
    {
        $grandparent = SelfLoopsVertexModel::create();
        $parent = SelfLoopsVertexModel::create();
        $child = SelfLoopsVertexModel::create();
        $grandchild = SelfLoopsVertexModel::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertEquals(
            [
                ['start_vertex' => $parent->id, 'end_vertex' => $parent->id],
                ['start_vertex' => $parent->id, 'end_vertex' => $child->id],
                ['start_vertex' => $parent->id, 'end_vertex' => $grandchild->id],
            ],
            $parent->descendantsWithSelf()->newPivotQuery()
                ->orderBy('end_vertex')
                ->get(['start_vertex', 'end_vertex'])
                ->map(fn($x) => (array)$x)
                ->toArray()
        );

        $this->assertEquals(
            [
                ['start_vertex' => $parent->id, 'end_vertex' => $child->id],
                ['start_vertex' => $parent->id, 'end_vertex' => $grandchild->id],
            ],
            $parent->descendants()->newPivotQuery()
                ->orderBy('end_vertex')
                ->get(['start_vertex', 'end_vertex'])
                ->map(fn($x) => (array)$x)
                ->toArray()
        );

        $this->assertEquals(
            [
                ['start_vertex' => $parent->id, 'end_vertex' => $child->id],
            ],
            $parent->children()->newPivotQuery()
                ->orderBy('end_vertex')
                ->get(['start_vertex', 'end_vertex'])
                ->map(fn($x) => (array)$x)
                ->toArray()
        );
    }
}
