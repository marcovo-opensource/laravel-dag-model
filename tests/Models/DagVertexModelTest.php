<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\EdgeNotFoundException;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManyInTransitiveClosureWithSelfUnion;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation1Vertex;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation7DlswVertex;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation7Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;

class DagVertexModelTest extends TestCase
{
    public function testParentsHasCorrectRelationName()
    {
        $this->assertSame('parents', Situation1Vertex::make()->parents()->getRelationName());
    }

    public function testChildrenHasCorrectRelationName()
    {
        $this->assertSame('children', Situation1Vertex::make()->children()->getRelationName());
    }

    public function testAncestorsHasCorrectRelationName()
    {
        $this->assertSame('ancestors', Situation1Vertex::make()->ancestors()->getRelationName());
    }

    public function testDescendantsHasCorrectRelationName()
    {
        $this->assertSame('descendants', Situation1Vertex::make()->descendants()->getRelationName());
    }

    public function testAncestorsWithSelfHasCorrectRelationName()
    {
        $relation = Situation1Vertex::make()->ancestorsWithSelf();
        $this->assertInstanceOf(BelongsToManyInTransitiveClosureWithSelfUnion::class, $relation);
        $this->assertSame('ancestorsWithSelf', $relation->getRelationName());
    }

    public function testDescendantsWithSelfHasCorrectRelationName()
    {
        $relation = Situation1Vertex::make()->descendantsWithSelf();
        $this->assertInstanceOf(BelongsToManyInTransitiveClosureWithSelfUnion::class, $relation);
        $this->assertSame('descendantsWithSelf', $relation->getRelationName());
    }

    public function testCanAttachParent()
    {
        $child = Situation1Vertex::create();
        $parent = Situation1Vertex::create();

        $child->parents()->attach($parent);

        $parents = $child->parents;
        $this->assertCount(1, $parents);
        $this->assertTrue($parent->is($parents->first()));
    }

    public function testCanAttachChild()
    {
        $child = Situation1Vertex::create();
        $parent = Situation1Vertex::create();

        $parent->children()->attach($child);

        $children = $parent->children;
        $this->assertCount(1, $children);
        $this->assertTrue($child->is($children->first()));
    }

    public function testCanDetachParent()
    {
        $child = Situation1Vertex::create();
        $parent1 = Situation1Vertex::create();
        $parent2 = Situation1Vertex::create();
        $parent3 = Situation1Vertex::create();

        $child->parents()->attach($parent1);
        $child->parents()->attach($parent2);
        $child->parents()->attach($parent3);

        $parents = $child->parents()->get();
        $this->assertCount(3, $parents);

        $child->parents()->detach($parent2);

        $parents = $child->parents()->orderBy('id')->get();
        $this->assertCount(2, $parents);
        $this->assertTrue($parent1->is($parents[0]));
        $this->assertTrue($parent3->is($parents[1]));
    }

    public function testCanDetachChild()
    {
        $parent = Situation1Vertex::create();
        $child1 = Situation1Vertex::create();
        $child2 = Situation1Vertex::create();
        $child3 = Situation1Vertex::create();

        $parent->children()->attach($child1);
        $parent->children()->attach($child2);
        $parent->children()->attach($child3);

        $children = $parent->children()->get();
        $this->assertCount(3, $children);

        $parent->children()->detach($child2);

        $children = $parent->children()->orderBy('id')->get();
        $this->assertCount(2, $children);
        $this->assertTrue($child1->is($children[0]));
        $this->assertTrue($child3->is($children[1]));
    }

    public function testCanDetachParents()
    {
        $child = Situation1Vertex::create();
        $parent1 = Situation1Vertex::create();
        $parent2 = Situation1Vertex::create();
        $parent3 = Situation1Vertex::create();

        $child->children()->attach(Situation1Vertex::create());
        $child->parents()->attach($parent1);
        $child->parents()->attach($parent2);
        $child->parents()->attach($parent3);

        $this->assertCount(3, $child->parents()->get());

        $child->parents()->sync([]);

        $this->assertCount(0, $child->parents()->get());
        $this->assertCount(1, $child->children()->get());
    }

    public function testCanDetachChildren()
    {
        $parent = Situation1Vertex::create();
        $child1 = Situation1Vertex::create();
        $child2 = Situation1Vertex::create();
        $child3 = Situation1Vertex::create();

        $parent->parents()->attach(Situation1Vertex::create());
        $parent->children()->attach($child1);
        $parent->children()->attach($child2);
        $parent->children()->attach($child3);

        $this->assertCount(3, $parent->children()->get());

        $parent->children()->sync([]);

        $this->assertCount(0, $parent->children()->get());
        $this->assertCount(1, $parent->parents()->get());
    }

    public static function vertex_model_data_provider(): array
    {
        return [
            'pathcount' => [Situation1Vertex::class],
            'dlsw' => [Situation7DlswVertex::class],
            'forest' => [Situation7Vertex::class],
        ];
    }

    /**
     * @dataProvider vertex_model_data_provider
     */
    #[DataProvider('vertex_model_data_provider')]
    public function test_cannot_attach_to_nonexistent(string $vertexClass)
    {
        /** @var DagVertexModel $parent */
        $parent = $vertexClass::create();
        /** @var DagVertexModel $child */
        $child = $vertexClass::create();
        $child->delete();

        $this->expectException(\Exception::class);
        if (env('DB_CONNECTION') === 'mariadb' && $vertexClass !== Situation7Vertex::class) {
            $this->expectExceptionMessage('Could not find inserted edge. Did you try to link a nonexistent vertex?');
        }
        $parent->children()->sync($child);
    }

    public function testAChildIsNotAParent()
    {
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $parent->children()->attach($child);

        $parents = $parent->parents()->get();
        $this->assertCount(0, $parents);

        $this->expectException(EdgeNotFoundException::class);
        $parent->parents()->detach($child);
    }

    public function testAParentIsNotAChild()
    {
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $child->parents()->attach($parent);

        $children = $child->children()->get();
        $this->assertCount(0, $children);

        $this->expectException(EdgeNotFoundException::class);
        $child->children()->detach($parent);
    }

    public function testCanGetParents()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $parents = $child->parents()->orderBy('id')->get();
        $this->assertCount(1, $parents);

        $this->assertTrue($parent->is($parents[0]));
    }

    public function testCanGetChildren()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $children = $parent->children()->orderBy('id')->get();
        $this->assertCount(1, $children);

        $this->assertTrue($child->is($children[0]));
    }

    public function testCanGetAncestors()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $ancestors = $child->ancestors()->orderBy('id')->get();
        $this->assertCount(2, $ancestors);

        $this->assertTrue($grandparent->is($ancestors[0]));
        $this->assertTrue($parent->is($ancestors[1]));
    }

    public function testCanGetDescendants()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $descendants = $parent->descendants()->orderBy('id')->get();
        $this->assertCount(2, $descendants);

        $this->assertTrue($child->is($descendants[0]));
        $this->assertTrue($grandchild->is($descendants[1]));
    }

    public function testCanGetAncestorsWithSelf()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $ancestors = $child->ancestorsWithSelf()->orderBy('id')->get();
        $this->assertCount(3, $ancestors);

        $this->assertTrue($grandparent->is($ancestors[0]));
        $this->assertTrue($parent->is($ancestors[1]));
        $this->assertTrue($child->is($ancestors[2]));
    }

    public function testCanGetDescendantsWithSelf()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $descendants = $parent->descendantsWithSelf()->orderBy('id')->get();
        $this->assertCount(3, $descendants);

        $this->assertTrue($parent->is($descendants[0]));
        $this->assertTrue($child->is($descendants[1]));
        $this->assertTrue($grandchild->is($descendants[2]));
    }

    public function testCanGetWhereHasAncestorsWithSelf()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertSame(
            [
                $parent->id,
                $child->id,
                $grandchild->id,
            ],
            Situation1Vertex::query()
                ->whereHas('ancestorsWithSelf', function (Builder $query) use ($parent) {
                    $query->whereKey($parent->id);
                })
                ->orderBy('id')
                ->pluck('id')
                ->all()
        );

        $this->assertSame(
            [
                $child->id,
                $grandchild->id,
            ],
            Situation1Vertex::query()
                ->whereHas('ancestors', function (Builder $query) use ($parent) {
                    $query->whereKey($parent->id);
                })
                ->orderBy('id')
                ->pluck('id')
                ->all()
        );
    }

    public function testCanGetWhereHasDescendantsWithSelf()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertSame(
            [
                $grandparent->id,
                $parent->id,
                $child->id,
            ],
            Situation1Vertex::query()
                ->whereHas('descendantsWithSelf', function (Builder $query) use ($child) {
                    $query->whereKey($child->id);
                })
                ->orderBy('id')
                ->pluck('id')
                ->all()
        );

        $this->assertSame(
            [
                $grandparent->id,
                $parent->id,
            ],
            Situation1Vertex::query()
                ->whereHas('descendants', function (Builder $query) use ($child) {
                    $query->whereKey($child->id);
                })
                ->orderBy('id')
                ->pluck('id')
                ->all()
        );
    }

    public function testCanQueryIsAncestorOf()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertTrue($grandparent->queryIsAncestorOf($grandchild));
        $this->assertTrue($parent->queryIsAncestorOf($grandchild));
        $this->assertTrue($child->queryIsAncestorOf($grandchild));
        $this->assertFalse($grandchild->queryIsAncestorOf($grandchild));

        $this->assertTrue($grandparent->queryIsAncestorOf($child));
        $this->assertTrue($parent->queryIsAncestorOf($child));
        $this->assertFalse($child->queryIsAncestorOf($child));
        $this->assertFalse($grandchild->queryIsAncestorOf($child));

        $this->assertTrue($grandparent->queryIsAncestorOf($parent));
        $this->assertFalse($parent->queryIsAncestorOf($parent));
        $this->assertFalse($child->queryIsAncestorOf($parent));
        $this->assertFalse($grandchild->queryIsAncestorOf($parent));

        $this->assertFalse($grandparent->queryIsAncestorOf($grandparent));
        $this->assertFalse($parent->queryIsAncestorOf($grandparent));
        $this->assertFalse($child->queryIsAncestorOf($grandparent));
        $this->assertFalse($grandchild->queryIsAncestorOf($grandparent));
    }

    public function testCanQueryIsDescendantOf()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertTrue($grandchild->queryIsDescendantOf($grandparent));
        $this->assertTrue($child->queryIsDescendantOf($grandparent));
        $this->assertTrue($parent->queryIsDescendantOf($grandparent));
        $this->assertFalse($grandparent->queryIsDescendantOf($grandparent));

        $this->assertTrue($grandchild->queryIsDescendantOf($parent));
        $this->assertTrue($child->queryIsDescendantOf($parent));
        $this->assertFalse($parent->queryIsDescendantOf($parent));
        $this->assertFalse($grandparent->queryIsDescendantOf($parent));

        $this->assertTrue($grandchild->queryIsDescendantOf($child));
        $this->assertFalse($child->queryIsDescendantOf($child));
        $this->assertFalse($parent->queryIsDescendantOf($child));
        $this->assertFalse($grandparent->queryIsDescendantOf($child));

        $this->assertFalse($grandchild->queryIsDescendantOf($grandchild));
        $this->assertFalse($child->queryIsDescendantOf($grandchild));
        $this->assertFalse($parent->queryIsDescendantOf($grandchild));
        $this->assertFalse($grandparent->queryIsDescendantOf($grandchild));
    }

    public function testCanQueryIsParentOf()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertFalse($grandparent->queryIsParentOf($grandchild));
        $this->assertFalse($parent->queryIsParentOf($grandchild));
        $this->assertTrue($child->queryIsParentOf($grandchild));
        $this->assertFalse($grandchild->queryIsParentOf($grandchild));

        $this->assertFalse($grandparent->queryIsParentOf($child));
        $this->assertTrue($parent->queryIsParentOf($child));
        $this->assertFalse($child->queryIsParentOf($child));
        $this->assertFalse($grandchild->queryIsParentOf($child));

        $this->assertTrue($grandparent->queryIsParentOf($parent));
        $this->assertFalse($parent->queryIsParentOf($parent));
        $this->assertFalse($child->queryIsParentOf($parent));
        $this->assertFalse($grandchild->queryIsParentOf($parent));

        $this->assertFalse($grandparent->queryIsParentOf($grandparent));
        $this->assertFalse($parent->queryIsParentOf($grandparent));
        $this->assertFalse($child->queryIsParentOf($grandparent));
        $this->assertFalse($grandchild->queryIsParentOf($grandparent));
    }

    public function testCanQueryIsChildOf()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertFalse($grandchild->queryIsChildOf($grandparent));
        $this->assertFalse($child->queryIsChildOf($grandparent));
        $this->assertTrue($parent->queryIsChildOf($grandparent));
        $this->assertFalse($grandparent->queryIsChildOf($grandparent));

        $this->assertFalse($grandchild->queryIsChildOf($parent));
        $this->assertTrue($child->queryIsChildOf($parent));
        $this->assertFalse($parent->queryIsChildOf($parent));
        $this->assertFalse($grandparent->queryIsChildOf($parent));

        $this->assertTrue($grandchild->queryIsChildOf($child));
        $this->assertFalse($child->queryIsChildOf($child));
        $this->assertFalse($parent->queryIsChildOf($child));
        $this->assertFalse($grandparent->queryIsChildOf($child));

        $this->assertFalse($grandchild->queryIsChildOf($grandchild));
        $this->assertFalse($child->queryIsChildOf($grandchild));
        $this->assertFalse($parent->queryIsChildOf($grandchild));
        $this->assertFalse($grandparent->queryIsChildOf($grandchild));
    }

    public function testCanCountRelations()
    {
        $grandparent = Situation1Vertex::create();
        $parent1 = Situation1Vertex::create();
        $parent2 = Situation1Vertex::create();
        $child1 = Situation1Vertex::create();
        $child2 = Situation1Vertex::create();
        $child3 = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent1);
        $grandparent->children()->attach($parent2);
        $parent1->children()->attach($child1);
        $parent1->children()->attach($child2);
        $parent1->children()->attach($child3);
        $parent2->children()->attach($child3);
        $child1->children()->attach($grandchild);

        Collection::make([
            $grandparent,
            $parent1,
            $parent2,
            $child1,
            $child2,
            $child3,
            $grandchild,
        ])->loadCount(['parents', 'children', 'descendants', 'ancestors', 'descendantsWithSelf', 'ancestorsWithSelf']);

        $this->assertSame(0, (int)$grandparent->ancestors_count);
        $this->assertSame(0, (int)$grandparent->parents_count);
        $this->assertSame(2, (int)$grandparent->children_count);
        $this->assertSame(6, (int)$grandparent->descendants_count);
        $this->assertSame(1, (int)$grandparent->ancestors_with_self_count);
        $this->assertSame(7, (int)$grandparent->descendants_with_self_count);

        $this->assertSame(1, (int)$parent1->ancestors_count);
        $this->assertSame(1, (int)$parent1->parents_count);
        $this->assertSame(3, (int)$parent1->children_count);
        $this->assertSame(4, (int)$parent1->descendants_count);
        $this->assertSame(2, (int)$parent1->ancestors_with_self_count);
        $this->assertSame(5, (int)$parent1->descendants_with_self_count);

        $this->assertSame(1, (int)$parent2->ancestors_count);
        $this->assertSame(1, (int)$parent2->parents_count);
        $this->assertSame(1, (int)$parent2->children_count);
        $this->assertSame(1, (int)$parent2->descendants_count);
        $this->assertSame(2, (int)$parent2->ancestors_with_self_count);
        $this->assertSame(2, (int)$parent2->descendants_with_self_count);

        $this->assertSame(2, (int)$child1->ancestors_count);
        $this->assertSame(1, (int)$child1->parents_count);
        $this->assertSame(1, (int)$child1->children_count);
        $this->assertSame(1, (int)$child1->descendants_count);
        $this->assertSame(3, (int)$child1->ancestors_with_self_count);
        $this->assertSame(2, (int)$child1->descendants_with_self_count);

        $this->assertSame(2, (int)$child2->ancestors_count);
        $this->assertSame(1, (int)$child2->parents_count);
        $this->assertSame(0, (int)$child2->children_count);
        $this->assertSame(0, (int)$child2->descendants_count);
        $this->assertSame(3, (int)$child2->ancestors_with_self_count);
        $this->assertSame(1, (int)$child2->descendants_with_self_count);

        $this->assertSame(3, (int)$child3->ancestors_count);
        $this->assertSame(2, (int)$child3->parents_count);
        $this->assertSame(0, (int)$child3->children_count);
        $this->assertSame(0, (int)$child3->descendants_count);
        $this->assertSame(4, (int)$child3->ancestors_with_self_count);
        $this->assertSame(1, (int)$child3->descendants_with_self_count);

        $this->assertSame(3, (int)$grandchild->ancestors_count);
        $this->assertSame(1, (int)$grandchild->parents_count);
        $this->assertSame(0, (int)$grandchild->children_count);
        $this->assertSame(0, (int)$grandchild->descendants_count);
        $this->assertSame(4, (int)$grandchild->ancestors_with_self_count);
        $this->assertSame(1, (int)$grandchild->descendants_with_self_count);
    }

    public function testDeletingAVertexDetachesParentsAndChildren()
    {
        $grandparent = Situation1Vertex::create();
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();
        $grandchild = Situation1Vertex::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertSame(6, DB::table('situation_1_edge')->count());

        $child->delete();

        $this->assertSame(1, DB::table('situation_1_edge')->count());

        $this->assertSame(0, $grandchild->ancestors()->count());
        $this->assertSame(0, $child->ancestors()->count());
        $this->assertSame(0, $child->descendants()->count());
        $this->assertSame(0, $parent->descendants()->count());

        $grandparentDescendants = $grandparent->descendants()->get();
        $parentParents = $parent->ancestors()->get();

        $this->assertCount(1, $grandparentDescendants);
        $this->assertCount(1, $parentParents);

        $this->assertTrue($parent->is($grandparentDescendants[0]));
        $this->assertTrue($grandparent->is($parentParents[0]));
    }

    public function testBelongsToManyInGraph()
    {
        $vertex = Situation1Vertex::create();

        $relation = $vertex->parents();

        $this->assertNotSame($relation->getParent(), $relation->getRelated());
    }

    public function testBelongsToManyInTransitiveClosure()
    {
        $vertex = Situation1Vertex::create();

        $relation = $vertex->ancestors();

        $this->assertNotSame($relation->getParent(), $relation->getRelated());
    }

    /**
     * Tests rebuilding a mutated diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     */
    public function testCanRebuildTransitiveClosure()
    {
        $vertex1 = Situation1Vertex::create();
        $vertex2 = Situation1Vertex::create();
        $vertex3 = Situation1Vertex::create();
        $vertex4 = Situation1Vertex::create();
        $vertex5 = Situation1Vertex::create();
        $vertex6 = Situation1Vertex::create();

        $edges = [
            [$vertex1->id, $vertex2->id],
            [$vertex1->id, $vertex3->id],
            [$vertex4->id, $vertex6->id],
            [$vertex2->id, $vertex5->id],
            [$vertex3->id, $vertex5->id],
            [$vertex2->id, $vertex4->id],
            [$vertex5->id, $vertex6->id],
        ];

        foreach ($edges as [$startVertex, $endVertex]) {
            DB::table('situation_1_edge')->insert([
                [
                    'start_vertex' => $startVertex,
                    'end_vertex' => $endVertex,
                    'path_count' => 0,
                    'edge_type' => IsEdgeInDagContract::TYPE_GRAPH_EDGE,
                ]
            ]);
        }

        (new Situation1Vertex())->rebuildTransitiveClosure();

        $edges = DB::table('situation_1_edge')
            ->select(['start_vertex AS s', 'end_vertex AS e', 'path_count AS c', 'edge_type AS t'])
            ->orderBy('start_vertex')
            ->orderBy('end_vertex')
            ->get()
            ->map(fn($x) => array_map('intval', (array)$x))
            ->toArray();

        $this->assertSame([
            ['s' => $vertex1->id, 'e' => $vertex2->id, 'c' => 1, 't' => 1],
            ['s' => $vertex1->id, 'e' => $vertex3->id, 'c' => 1, 't' => 1],
            ['s' => $vertex1->id, 'e' => $vertex4->id, 'c' => 1, 't' => 0],
            ['s' => $vertex1->id, 'e' => $vertex5->id, 'c' => 2, 't' => 0],
            ['s' => $vertex1->id, 'e' => $vertex6->id, 'c' => 3, 't' => 0],
            ['s' => $vertex2->id, 'e' => $vertex4->id, 'c' => 1, 't' => 1],
            ['s' => $vertex2->id, 'e' => $vertex5->id, 'c' => 1, 't' => 1],
            ['s' => $vertex2->id, 'e' => $vertex6->id, 'c' => 2, 't' => 0],
            ['s' => $vertex3->id, 'e' => $vertex5->id, 'c' => 1, 't' => 1],
            ['s' => $vertex3->id, 'e' => $vertex6->id, 'c' => 1, 't' => 0],
            ['s' => $vertex4->id, 'e' => $vertex6->id, 'c' => 1, 't' => 1],
            ['s' => $vertex5->id, 'e' => $vertex6->id, 'c' => 1, 't' => 1],
        ], $edges);
    }
}
