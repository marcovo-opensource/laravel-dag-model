<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation1Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class LinkingTest extends TestCase
{
    /**
     * Build a diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     *
     * @return Situation1Vertex[]
     */
    private function buildDiagonalDiamond(): array
    {
        $V1 = Situation1Vertex::create();
        $V2 = Situation1Vertex::create();
        $V3 = Situation1Vertex::create();
        $V4 = Situation1Vertex::create();
        $V5 = Situation1Vertex::create();
        $V6 = Situation1Vertex::create();

        $V1->children()->attach($V2);
        $V1->children()->attach($V3);
        $V2->children()->attach($V4);
        $V2->children()->attach($V5);
        $V3->children()->attach($V5);
        $V4->children()->attach($V6);
        $V5->children()->attach($V6);

        $V1->refresh();
        $V2->refresh();
        $V3->refresh();
        $V4->refresh();
        $V5->refresh();
        $V6->refresh();

        return [$V1, $V2, $V3, $V4, $V5, $V6];
    }

    public function testCanLinkDescendantChildren()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $queries = 0;
        DB::listen(function () use (&$queries) { $queries++; });
        $V1->linkDescendantChildren();
        $this->assertSame(2, $queries);

        $V2 = $V1->descendants->find($V2);
        $V3 = $V1->descendants->find($V3);
        $V4 = $V1->descendants->find($V4);
        $V5 = $V1->descendants->find($V5);
        $V6 = $V1->descendants->find($V6);

        $this->assertSame([$V2, $V3], $V1->children->sortBy('id')->values()->all());
        $this->assertSame([$V4, $V5], $V2->children->sortBy('id')->values()->all());
        $this->assertSame([$V5], $V3->children->sortBy('id')->values()->all());
        $this->assertSame([$V6], $V4->children->sortBy('id')->values()->all());
        $this->assertSame([$V6], $V5->children->sortBy('id')->values()->all());
        $this->assertSame([], $V6->children->sortBy('id')->values()->all());

        $this->assertSame(2, $queries);
    }

    public function testCanLinkDescendantDescendants()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $queries = 0;
        DB::listen(function () use (&$queries) { $queries++; });
        $V1->linkDescendantDescendants();
        $this->assertSame(2, $queries);

        $V2 = $V1->descendants->find($V2);
        $V3 = $V1->descendants->find($V3);
        $V4 = $V1->descendants->find($V4);
        $V5 = $V1->descendants->find($V5);
        $V6 = $V1->descendants->find($V6);

        $this->assertSame([$V2, $V3, $V4, $V5, $V6], $V1->descendants->sortBy('id')->values()->all());
        $this->assertSame([$V4, $V5, $V6], $V2->descendants->sortBy('id')->values()->all());
        $this->assertSame([$V5, $V6], $V3->descendants->sortBy('id')->values()->all());
        $this->assertSame([$V6], $V4->descendants->sortBy('id')->values()->all());
        $this->assertSame([$V6], $V5->descendants->sortBy('id')->values()->all());
        $this->assertSame([], $V6->descendants->sortBy('id')->values()->all());

        $this->assertSame(2, $queries);
    }

    public function testCanLinkAncestorParents()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $queries = 0;
        DB::listen(function () use (&$queries) { $queries++; });
        $V6->linkAncestorParents();
        $this->assertSame(2, $queries);

        $V1 = $V6->ancestors->find($V1);
        $V2 = $V6->ancestors->find($V2);
        $V3 = $V6->ancestors->find($V3);
        $V4 = $V6->ancestors->find($V4);
        $V5 = $V6->ancestors->find($V5);

        $this->assertSame([], $V1->parents->sortBy('id')->values()->all());
        $this->assertSame([$V1], $V2->parents->sortBy('id')->values()->all());
        $this->assertSame([$V1], $V3->parents->sortBy('id')->values()->all());
        $this->assertSame([$V2], $V4->parents->sortBy('id')->values()->all());
        $this->assertSame([$V2, $V3], $V5->parents->sortBy('id')->values()->all());
        $this->assertSame([$V4, $V5], $V6->parents->sortBy('id')->values()->all());

        $this->assertSame(2, $queries);
    }

    public function testCanLinkAncestorAncestors()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $queries = 0;
        DB::listen(function () use (&$queries) { $queries++; });
        $V6->linkAncestorAncestors();
        $this->assertSame(2, $queries);

        $V1 = $V6->ancestors->find($V1);
        $V2 = $V6->ancestors->find($V2);
        $V3 = $V6->ancestors->find($V3);
        $V4 = $V6->ancestors->find($V4);
        $V5 = $V6->ancestors->find($V5);

        $this->assertSame([], $V1->ancestors->sortBy('id')->values()->all());
        $this->assertSame([$V1], $V2->ancestors->sortBy('id')->values()->all());
        $this->assertSame([$V1], $V3->ancestors->sortBy('id')->values()->all());
        $this->assertSame([$V1, $V2], $V4->ancestors->sortBy('id')->values()->all());
        $this->assertSame([$V1, $V2, $V3], $V5->ancestors->sortBy('id')->values()->all());
        $this->assertSame([$V1, $V2, $V3, $V4, $V5], $V6->ancestors->sortBy('id')->values()->all());

        $this->assertSame(2, $queries);
    }
}
