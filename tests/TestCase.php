<?php

namespace Marcovo\LaravelDagModel\Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Application;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Orchestra\Testbench\Database\MigrateProcessor;

class TestCase extends BaseTestCase
{
    protected bool $checkSelfLoops = false;

    private static bool $migrated = false;

    public static function isGitlabCiServer(): bool
    {
        return !empty(env('CI_PROJECT_ID'));
    }

    public function beginDatabaseTransaction()
    {
        if (! self::$migrated || env('DB_CONNECTION') === 'sqlite') {
            $this->loadTestMigrations();

            $this->resetApplicationArtisanCommands($this->app);

            self::$migrated = true;
        }

        parent::beginDatabaseTransaction();
    }

    protected function refreshInMemoryDatabase()
    {
        // Laravel <=10
        parent::refreshInMemoryDatabase();

        $this->loadTestMigrations();

        $this->app[Kernel::class]->setArtisan(null);
    }

    protected function restoreInMemoryDatabase()
    {
        // Laravel 11
        parent::restoreInMemoryDatabase();

        $this->loadTestMigrations();

        $this->app[Kernel::class]->setArtisan(null);
    }

    private function loadTestMigrations()
    {
        $migrator = new MigrateProcessor($this, [
            '--path' => __DIR__ . '/database/migrations',
            '--realpath' => true,
        ]);
        $migrator->up();
    }

    public function getEnvironmentSetUp($app)
    {
        $app['config']->set('app.debug', 'true');

        $app['config']->set('database.default', 'testbench');

        $dbConnection = env('DB_CONNECTION');
        if ($dbConnection === 'mariadb') {
            $app['config']->set('database.connections.testbench', [
                'driver' => version_compare(Application::VERSION, '11', '>=') ? 'mariadb' : 'mysql',
                'host' => env('DB_MYSQL_HOST', 'laravel-dag-model-mariadb-server-10-7'),
                'username' => 'laravel-dag-model',
                'password' => 'secret',
                'database' => 'laravel-dag-model',
                'prefix' => '',
            ]);
        } elseif ($dbConnection === 'postgresql') {
            $app['config']->set('database.connections.testbench', [
                'driver' => 'pgsql',
                'host' => env('DB_PGSQL_HOST', 'laravel-dag-model-postgresql-server-10'),
                'username' => 'postgres',
                'password' => 'secret',
                'database' => 'postgres',
                'prefix' => '',
            ]);
        } elseif ($dbConnection === 'sqlite') {
            $app['config']->set('database.connections.testbench', [
                'driver' => 'sqlite',
                'database' => ':memory:',
                'prefix' => '',
            ]);
            DB::statement("PRAGMA foreign_keys = ON;");
        } else {
            throw new \Exception('Unknown phpunit DB connection');
        }
    }

    /**
     * Assert that the actual edges are the same as the edges in the graph plus the edges in the
     * transitive closure that are not in the graph plus possibly the self-loop edges
     * @param array $graphEdges
     * @param array $tcEdges
     * @param array $slEdges
     * @param array|Collection $actual
     * @return void
     * @throws LaravelDagModelException
     */
    protected function assertEdges(array $graphEdges, array $tcEdges, array $slEdges, $actual)
    {
        $actual = collect($actual);

        $groups = $actual->groupBy('edge_type');
        if ($groups->keys()->diff([0, 1, 2])->count() > 0) {
            throw new LaravelDagModelException('Found invalid graph edge values: ' . $groups->keys()->diff([0, 1])->implode(','));
        }

        $actualTcEdges = $groups[0] ?? collect();
        $actualGraphEdges = $groups[1] ?? collect();
        $actualSelfLoopEdges = $groups[2] ?? collect();

        $this->assertSameEdges($graphEdges, $actualGraphEdges);
        $this->assertSameEdges($tcEdges, $actualTcEdges);

        if ($this->checkSelfLoops) {
            $this->assertSameEdges(
                collect($slEdges)->map(fn($x) => [$x, $x])->toArray(),
                $actualSelfLoopEdges
            );
        }
    }

    protected function assertSameEdges(array $expectedEdges, $actualEdges)
    {
        if (count($expectedEdges) === 0) {
            $this->assertCount(0, $actualEdges);
            return;
        }

        $firstRow = collect($expectedEdges)->first();
        unset($firstRow[0]);
        unset($firstRow[1]);
        $allKeys = $textKeys = array_keys($firstRow);
        array_unshift($allKeys, 'start_vertex', 'end_vertex');

        $sortFunction = fn ($edge) => $edge['start_vertex'] . '-' . $edge['end_vertex'];

        $expectedEdges = collect($expectedEdges)
            ->map(function ($edge) use ($textKeys) {
                $return = [];
                $return['start_vertex'] = (string)$edge[0];
                $return['end_vertex'] = (string)$edge[1];

                foreach ($textKeys as $textKey) {
                    $return[$textKey] = $edge[$textKey];
                }

                return $return;
            })
            ->sortBy($sortFunction)
            ->values()
            ->toArray();

        $actualEdges = collect($actualEdges)
            ->map(function ($row) use ($allKeys) {
                $return = collect($row)->only($allKeys)->map(fn ($x) => intval($x));
                $return['start_vertex'] = (string)$row->start_vertex;
                $return['end_vertex'] = (string)$row->end_vertex;
                return $return;
            })
            ->sortBy($sortFunction)
            ->values()
            ->toArray();

        $this->assertSame($expectedEdges, $actualEdges);
    }

    protected function withVertices($vertices)
    {
        if (!$this->checkSelfLoops) {
            return;
        }

        $algorithm = $this->getAlgorithm();

        foreach ($vertices as $vertex) {
            $algorithm->createSelfLoopEdge($vertex);
        }
    }
}
