# Getting started

- **[Getting started](getting-started.md)**
  - [Step 1) Database & migration](#user-content-step-1-database-migration)
  - [Step 2) Model modifications](#user-content-step-2-model-modifications)
  - [Step 3) Creating the pivot model](#user-content-step-3-creating-the-pivot-model)
  - [Step 4) Migrating existing data](#user-content-step-4-migrating-existing-data)
  - [Done!](#user-content-done)
- [Important notes](important.md)
- API
  - [General](api-general.md)
  - [Max Vertex Degree](api-max-vertex-degree.md)
  - [Forest (trees)](api-forest.md)
  - [Topological ordering](api-topological-ordering.md)
  - [Self loops](api-self-loops.md)

## Step 1) Database & migration
This package assumes you have some model, backed by a database table, which we will call the **vertex model**.
This package requires that the vertex model at least contains a primary key.

Besides the vertex model, this package requires an additional pivot table called the **edge model**.
The edge model contains the relations between vertices.
As described in [Important notes](important.md), you should choose between using the `PathCountAlgorithm`, `DlswAlgorithm` or `ForestAlgorithm`.
For each of these algorithms, an edge model migration stub is available in the [`database/migrations/`](../database/migrations/) directory.

**&rArr; Choose your algorithm and copy the corresponding migration into your migrations folder**

The various columns present in these migrations are all mandatory, however you can rename them as desired by defining their alternate names in the edge model.
All algorithms have the following columns in common:
 - `start_vertex` - The model/vertex this edge originates from (the parent)
 - `end_vertex` - The model/vertex this edge points towards (the child)
 - `edge_type` - Whether this edge is a graph edge (`1`, *direct edge*), a transitive closure edge (`0`, *indirect edge* composed of multiple graph edges) or a self-loop edge (`2`, only when using `WithSelfLoops` extension)

## Step 2) Model modifications
Once you have configured and executed the migration, we can plug in the DAG model functionality into your vertex model.
You can either extend from the `DagVertexModel` or use the `IsVertexInDag` trait in your own model.
An example vertex model would look like:

```php
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

class MyVertexModel extends DagVertexModel
{
    protected $table = 'my_vertex_table';

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new MyEdgeModel();
    }
}
```
Or, when using the trait directly:
```php
use Illuminate\Database\Eloquent\Model;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDag;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

class MyVertexModel extends Model implements IsVertexInDagContract
{
    use IsVertexInDag;
    
    protected $table = 'my_vertex_table';

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new MyEdgeModel();
    }
}
```

The `getEdgeModel()` method should return an (empty) instance of your edge pivot model you will create in step 3.

**&rArr; Modify your vertex model by using the described class or trait**

## Step 3) Creating the pivot model
Your edge pivot model should extend one of the [edge classes](important.md#user-content-transitive-closure-algorithms) `PathCountAlgorithmEdge`, `DlswAlgorithmEdge` or `ForestAlgorithmEdge`, or you could define your own by using the `IsEdgeInDag` trait and implementing the `getTransitiveClosureAlgorithm()` method yourself.
An example of extending one of the given edge classes:
```php
use Marcovo\LaravelDagModel\Models\Edge\PathCountAlgorithmEdge;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;

class MyEdgeModel extends PathCountAlgorithmEdge
{
    protected $table = 'my_edge_table';

    public function getVertexModel(): IsVertexInDagContract
    {
        return new MyVertexModel();
    }
}
```
Note that `getVertexModel()` should return an (empty) instance of your vertex model.

When defining your own edge model from scratch, your class should look similar to:
```php
use Illuminate\Database\Eloquent\Relations\Pivot;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDag;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\DlswAlgorithm;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

class MyEdgeModel extends Pivot implements IsEdgeInDagContract
{
    use IsEdgeInDag;
    
    protected $table = 'my_edge_table';

    public $incrementing = true;

    public function getTransitiveClosureAlgorithm(): TransitiveClosureAlgorithmContract
    {
        return new DlswAlgorithm($this);
    }
    
    public function getVertexModel(): IsVertexInDagContract
    {
        return new MyVertexModel();
    }
}
```

**&rArr; Create your edge pivot model, and specify it in your vertex model `getEdgeModel()` method**

## Step 4) Migrating existing data
This step only applies if you need to migrate existing data.
When this is the case, follow the following steps:
1. Make sure you migrate your relations (if you have any) into the edge table.
In this table, you need to fill the `start_vertex` and `end_vertex` columns, and set `edge_type` to `1` (or to `0` for transitive closure edges, if you already have those).
2. If you have multiple independent graphs in one table, make sure to define `getSeparationCondition()` accordingly as described in the [API documentation](api-general.md#user-content-method-getseparationcondition).
3. After that, you can (re)construct the transitive closure using the `rebuildTransitiveClosure()` method, also described in the [API documentation](api-general.md#user-content-method-rebuildtransitiveclosure).

## Done!
Now you are ready to start using the DAG functionality.
You can add edges using e.g. `$model->children()->attach($child)` and remove edges using e.g. `$model->children()->detach($child)`.
More details are described in the [API documentation](api-general.md).
