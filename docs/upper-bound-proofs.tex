\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}

\title{Some results on the Transitive Closure of Directed Acyclic Graphs}
\author{Marco van Oort}
\date{March 2022}

\begin{document}

\maketitle

\section{Maximum number of edges in general DAGs}

It is well-known that a graph $G$ with $N$ vertices can have at most $O(N^2)$ edges. Not allowing $1$-cycles, we specifically have maxima of e.g. $N(N-1)$ in the general directed case, and $\frac{N(N-1)}{2}$ for undirected graphs and directed acyclic graphs (DAGs).

\begin{lemma}
Suppose a DAG $G$ with $N$ vertices has $\frac{N(N-1)}{2}$ edges. Then, $G$ is exactly the graph containing edges $v_i \rightarrow v_j$ for all $1 \leq i < j \leq N$, possibly after reordering indices $v_i$.
\end{lemma}

\begin{proof}
Consider the undirected graph $G'$ with the same edges as $G$, except being undirected. By the number of edges in the graph, it is seen that $G'$ is a complete graph. Consider the procedure of converting $G'$ into a directed acyclic graph $G''$. We start with the DAG $G''$ containing the same vertices as $G$ but no edges.

As vertices in $G'$ are indistiguishable, without loss of generality we can start at vertex $v_1$ and choose another vertex $v_2$, and add an edge $v_1 \rightarrow v_2$ between them in $G''$. Note that this collection of one edge is described by $v_i \rightarrow v_j$ for all $1 \leq i < j \leq k$ for $k = 2$.

We proceed by induction. Suppose all edges in $G''$ are described by $v_i \rightarrow v_j$ for all $1 \leq i < j \leq k - 1$ for some $k \in [3, N]$. Again without loss of generality we can pick $v_k$ as the next vertex to add to $G''$ (or else, the vertices $v_l | l \in [k, N]$ can be reordered arbitrarily so any of these vertices may be chosen to be $v_k$). We consider adding edges between $v_k$ and $v_i$ for $i \in [1, k-1]$.

First suppose we choose not to add any edges $v_k \rightarrow v_i$ for $i \in [1, k-1]$. Then, we must add edges $v_i \rightarrow v_k$ for $i \in [1, k-1]$ and we can conclude all edges are now described by $v_i \rightarrow v_j$ for all $1 \leq i < j \leq k$.

Otherwise, let $m \in [1, k-1]$ be the lowest vertex index for which we add edge $v_k \rightarrow v_m$. As we have edges $v_m \rightarrow v_i$ for all $i \in [m+1, k-1]$, by the acyclic property we may not add any edges $v_i \rightarrow v_k$ for $i \in [m+1, k-1]$. Hence by adding $v_k \rightarrow v_m$ we also induce $v_k \rightarrow v_i$ for $i \in [m+1, k-1]$. Also by the definition of $m$ we do not add edges $v_k \rightarrow v_i$ for $i \in [1, m-1]$. Hence, we must add edges $v_i \rightarrow v_k$ for $i \in [1, m-1]$. To sum up, we add the following edges:
\begin{align}
    v_i &\rightarrow v_k \quad \forall i \in [1, m-1] \\
    v_k &\rightarrow v_m \\
    v_k &\rightarrow v_i \quad \forall i \in [m+1, k-1]
\end{align}
Now renumber the vertices $v_j$ with $j \in [m+1, k-1]$ by adding 1 to them and renumber $v_k$ to $v_m$. Then, we again have $v_i \rightarrow v_j$ for all $1 \leq i < j \leq k$.
\end{proof}

\begin{corollary}
Suppose a DAG $G$ has a transitive closure containing $\frac{N(N-1)}{2}$ edges. Then, possibly after renumbering the vertices, the transitive reduction of $G$ is a chain of $N - 1$ edges $v_1 \rightarrow v_2$, \ldots, $v_{n-1} \rightarrow v_n$.
\end{corollary}

\section{Maximum number of edges in the transitive closure of DAGs with a fixed height}

Define the height $p$ of a DAG $G$ to be the maximum directed path length within $G$. In other words, define $length(i, j)$ to be the number of edges in the longest path from vertex $i$ to $j$ if such a path exists, or $0$ otherwise. Then $p = \max_{i, j \leq N} length(i, j)$.

Denote the indegree of vertex $v_i$ as $\hat{d}_i$ and the maximum indegree as $\hat{d} = \max \hat{d}_i$.

\begin{lemma}\label{lemma:indegreeupperbound}
Consider a DAG $G$ with $N$ vertices, height $p$ and maximum indegree $\hat{d}$. Then, an upper bound on the number of edges in the transitive closure $T$ of $G$ is given by $pN$ if $\hat{d} = 1$ or $\frac{\hat{d}(\hat{d}^p - 1)}{\hat{d} - 1}N$ if $\hat{d} > 1$.
\end{lemma}

\begin{proof}
Consider a single vertex $v_i$ in $G$. This vertex has at most $\hat{d}$ parents, contributing $\hat{d}$ edges to $T$. Each of the $\hat{d}$ parents can have up to $\hat{d}$ parents too, contributing $\hat{d}^2$ edges to $v_i$ in $T$. In general, $T$ can contain up to $\hat{d}^q$ edges from $q$'th order ancestors of $v_i$, for $q \in [1, p]$. An upper bound for the total number of ancestor edges in $T$ to vertex $v_i$ is hence given by
\begin{equation}
    \sum_{q=1}^{p} \hat{d}^q = \begin{cases}
			p, & \text{if $\hat{d} = 1$}\\
            \frac{\hat{d}(\hat{d}^p - 1)}{\hat{d} - 1}, & \text{if $\hat{d} > 1$.}
		 \end{cases}
\end{equation}
Multiplying this with the number of vertices $N$ yields the stated result.
\end{proof}

Note that by defining $G'$ the same as $G$ except with reversed edge arrows, we can apply Lemma \ref{lemma:indegreeupperbound} again but then on the maximum outdegree instead of the maximum indegree. Both of the upper bounds hold for $G$, hence the lowest of the two degrees provides the best upper bound. Define the parent degree $d$ as the minimum of the maximum indegree and the maximum outdegree. Then we have the following result.

\begin{theorem}\label{theorem:parentdegreeupperbound}
Consider a DAG $G$ with $N$ vertices, height $p$ and parent degree $d$. Then, an upper bound on the number of edges in the transitive closure $T$ of $G$ is given by $pN$ if $d = 1$ or $\frac{d(d^p - 1)}{d - 1}N$ if $d > 1$.
\end{theorem}

\begin{proof}
Follows from the preceding note and Lemma \ref{lemma:indegreeupperbound}.
\end{proof}

\begin{corollary}
The transitive closure of a forest (or reversed forest) with $N$ vertices and height $p$ has at most $pN$ edges.
\end{corollary}

\begin{proof}
Note that a DAG is a (reversed) forest if and only if the parent degree is $1$. The result now follows from Theorem \ref{theorem:parentdegreeupperbound}.
\end{proof}

\begin{corollary}
Consider a DAG $G$ with $N$ vertices, height $p$ and parent degree $d > 1$ (i.e., not a (reversed) forest). Then the maximum number of edges in the transitive closure of $G$ is $O(m^pN)$ for some $m \in \mathbb{R}_{[1, d]}$.
\end{corollary}

\begin{proof}
This follows from Theorem \ref{theorem:parentdegreeupperbound}, with the observation that the upperbound in that proof assumes that every vertex has maximum indegree. Often this will not be the case, and the effective value of $m$ can be lower. The actual value of $m$ is dependent on the structure within the graph so no formulas for $m$ are provided.
\end{proof}

In the preceding results we have derived some upper bounds on the number of edges in the transitive closure of DAGs. Note that by the constructions presented here, these upper bounds are likely not tight, hence with more analysis better (but probably more complicated) upper bounds could be possible.
\end{document}
