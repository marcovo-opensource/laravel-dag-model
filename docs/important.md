# Important notes

- [Getting started](getting-started.md)
- **[Important notes](important.md)**
  - [Transitive closure algorithms](#user-content-transitive-closure-algorithms)
  - [Caution](#user-content-caution)
  - [Space & run-time complexity](#user-content-space-run-time-complexity)
- API
  - [General](api-general.md)
  - [Max Vertex Degree](api-max-vertex-degree.md)
  - [Forest (trees)](api-forest.md)
  - [Topological ordering](api-topological-ordering.md)
  - [Self loops](api-self-loops.md)

## Transitive closure algorithms
This package allows you to turn any database model into a DAG, using a `BelongsToMany` pivot table.
It provides functionality for easily and efficiently adding and deleting relations between models, as well as for querying parent/child models or ancestor/descendant models.
In order to do this, besides the graph itself it also maintains the **transitive closure** (TC) of the DAG in the pivot table.
Three algorithms are included that can manage the TC:
- The `PathCountAlgorithm` algorithm maintains a `path_count` column in the pivot/edge table that for each edge contains the number of paths in the DAG that constitute the presence of that edge.
- The `DlswAlgorithm` algorithm is based on the algorithms described in the [paper](https://homepages.inf.ed.ac.uk/libkin/papers/tc-sql.pdf) by Dong, Libkin, Su & Wong.
- The `ForestAlgorithm` is a specialization of the `PathCountAlgorithm` especially suited for handling forests. In this case, the `path_count` column is not needed and various queries can be simplified. This is the recommended algorithm to use for forests (/trees).

Compared to the `DlswAlgorithm`, the `PathCountAlgorithm` requires one column of extra storage, and an additional processing step when adding an edge between two vertices/models.
However, in turn the `PathCountAlgorithm` does not require the three `TEMPORARY TABLE`s that the `DlswAlgorithm` requires for deleting an edge between vertices.
In a limited amount of testing with php 7.4 and MariaDB, the `PathCountAlgorithm` proved to be faster than the `DlswAlgorithm` algorithm.

However, do note that `PathCountAlgorithm` has an additional limitation: it can only store path counts up to `~2^63`.
When dealing with graphs with large path counts (such as [64 chained diamonds](tests/TransitiveClosureAlgorithms/PathCountAlgorithm/CreateTest.php) (`test_fails_when_potential_path_count_overflow_is_detected()`)), the path count overflows and will break the algorithm.
Hence, an additional check is built in to ensure the path count never overflows, in which case an exception will be thrown.
Note that while `ForestAlgorithm` is a specialization of the `PathCountAlgorithm` algorithm, the `ForestAlgorithm` does not suffer this limitation due to the lack of the need for the `path_count` column.

## Caution
- Adding and removing vertices/nodes/models in the graph should be done using model methods, not query builder methods. When doing a `query()->insert()` or `query()->delete()`, the transitive closure will not be updated correctly
- Only the algorithms may insert and delete rows from the pivot table. You may include additional columns in the pivot table and use these yourself, but you should not modify any of the provided algorithm columns.
- Some relations introduced by this package should be considered read-only and hence should not be used to update the graph. Refer to the specific documentation on the various relations for more details.
- As outlined in [Space & run-time complexity](#user-content-space-run-time-complexity), this DAG implementation works best for shallow graphs, i.e. graphs with a bounded maximum height (path length). One ideal example would be a graph used to define some sort of configuration, which is set up once initially and thereafter only mutated occasionally.
  If the graph, and in particular the maximum path length, grows steadily with time (comparable to a `git` graph), the storage required may scale as `O(N^2)`, which could be undesirable. In such a case:
  - The `PathCountAlgorithm` may break if your graph contains a lot of branching and merging (see [Transitive closure algorithms](#user-content-transitive-closure-algorithms)).
  - The `DlswAlgorithm` and `ForestAlgorithm` will continue to work.
  - In case of a steadily growing tree, you may consider using [kalnoy/nestedset](https://packagist.org/packages/kalnoy/nestedset) instead, which only requires `O(N)` additional storage.

## Space & run-time complexity
In the following, we write `N` for the number of model entries (vertices).

### Space complexity
In general, the number of rows in the pivot table (vertices in the transitive closure) may increase up to `N*(N-1)`, or `O(N^2)`.
Improved upper bounds are given in Theorem 2.2 and its corollaries of the included [mathematical analysis](https://gitlab.com/api/v4/projects/34045887/packages/generic/upper-bound-proofs-pdf/v0.0/upper-bound-proofs.pdf).
In particular, denote the maximum height (path length) in your DAG with `p`.
- If your DAG is a forest, for large `N` the number of pivot table rows (edges) grows as `O(pN)`.
- Otherwise, if your DAG is not a forest, denote the maximum number of parents per vertex/model (or maximum number of children, either of which is lowest) with `d`.
  Then for large `N` the number of pivot table rows (edges) grows as `O(m^p * N)` for some `m` between `1` and `d`.

For large `N` these upper bounds may provide better insight in the long-term growth of your DAG pivot table considering the use-case.

### Time complexity
In the paper on the `DlswAlgorithm` referenced before, in chapter 5 it is argued that the time complexity of its add/delete algorithms is `O(N * log(N))`.
By the same analysis and the observation that `PathCountAlgorithm` performs different but similar queries, we can conclude that the time complexity of its add/delete algorithms also is `O(N * log(N))`.
