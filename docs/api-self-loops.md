# Public API - Self loops extension

- [Getting started](getting-started.md)
- [Important notes](important.md)
- API
  - [General](api-general.md)
  - [Max Vertex Degree](api-max-vertex-degree.md)
  - [Forest (trees)](api-forest.md)
  - [Topological ordering](api-topological-ordering.md)
  - **[Self loops](api-self-loops.md)**
    - [Description](#user-content-description)
    - [Trait `WithSelfLoops`](#user-content-trait-withselfloops)

`WithSelfLoops` - *A trait for your vertex model, for better `****WithSelf()` support.*

## Description
By default, `DagVertexModel` provides two relations `ancestorsWithSelf()` and `descendantsWithSelf()`, which use an SQL `UNION` to include the `self` vertex.
While this approach is valid, it does prohibit any pivot conditions to be applied.
The `WithSelfLoops` extension provides an alternative implementation of these methods which allows you to fully utilize Laravel's base `BelongsToMany` functionality.
In order to do so, `WithSelfLoops` utilizes [self-loops](https://en.wikipedia.org/wiki/Loop_(graph_theory)), i.e. it stores one self-loop (an additional edge) per vertex in the graph.
The relation queries performed by `WithSelfLoops` might also execute slightly faster than the native methods due to more efficient indexing.

> Note: When your model extends the `DagVertexModel`, you can use the `WithSelfLoops` trait without problems. When on the other hand your model uses the `IsVertexInDag` trait directly, collisions will occur when using the `WithSelfLoops` trait. Make sure in this case you `use` any methods from the `WithSelfLoops` trait `insteadof` those in `IsVertexInDag`.

## Trait `WithSelfLoops`

### Method `rebuildSelfLoops()`
You may wish to rebuild the self loops from the graph definition.
Situations in which this may be needed is when the list of self loops for some reason is corrupted, or when migrating from another data structure or DAG implementation.
You can rebuild the self loop using this method on a fully configured vertex model instance.
The rebuild algorithm adds any missing self loop edges.
Valid existing edges remain unchanged.
