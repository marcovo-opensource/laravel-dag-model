# Public API - Max vertex degree extension

- [Getting started](getting-started.md)
- [Important notes](important.md)
- API
  - [General](api-general.md)
  - **[Max Vertex Degree](api-max-vertex-degree.md)**
    - [Description](#user-content-description)
    - [Trait `WithMaxVertexDegree`](#user-content-trait-withmaxvertexdegree)
  - [Forest (trees)](api-forest.md)
  - [Topological ordering](api-topological-ordering.md)
  - [Self loops](api-self-loops.md)

`WithMaxVertexDegree` - *A trait for your vertex model, providing the ability to limit the number of parents or children of a vertex.*

## Description
The `WithMaxVertexDegree` trait provides functionality for enforcing a maximum indegree or outdegree on vertices.
A maximum indegree enforces a maximum number of parents per vertex, while a maximum outdegree enforces a maximum number of children per vertex.
With a maximum indegree of `1`, the graph specializes into a [forest (collection of trees)](api-forest.md).

## Trait `WithMaxVertexDegree`

### Method `maxInDegree`
Override this method to enforce a maximum indegree.
A value of `null` implies no maximum is enforced.
```php
    public function maxInDegree(): ?int
    {
        return 1; // Forest
    }
```

### Method `maxOutDegree`
Override this method to enforce a maximum outdegree.
A value of `null` implies no maximum is enforced.
```php
    public function maxOutDegree(): ?int
    {
        return 2; // Together with indegree=1, makes a forest of binary trees
    }
```
