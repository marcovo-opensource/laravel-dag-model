<?php

namespace Marcovo\LaravelDagModel\Helpers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Models\Extensions\IsForest;

/**
 * @internal
 */
class LinkingHelper
{
    /**
     * Eager load all (e.g.) children of the given vertex's (e.g.) descendants. Additionally, replace any vertex
     * instance within these (e.g.) children arrays by the exact same instance of the model, such that when recursing
     * over the graph, whenever the same vertex is encountered, it is guaranteed to be the very same object instance.
     * Note that this is done one-way only, so only when using the (e.g.) children relation.
     */
    public static function linkDirectToIndirect(Model $vertex, string $indirectRelation, string $directRelation)
    {
        // Load all (e.g.) descendants
        $vertex->loadMissing($indirectRelation);

        // Load all (e.g.) children of all (e.g.) descendants
        /** @var Collection|Model[] $indirectAndSelf */
        $indirectAndSelf = clone $vertex->$indirectRelation;
        $indirectAndSelf[] = $vertex;

        $verticesByKey = $indirectAndSelf->keyBy($vertex->getKeyName());

        $indirectAndSelf->loadMissing($directRelation . ':' . $vertex->getKeyName());

        // For the given vertex and all (e.g.) descendants, set the (e.g.) children relation using
        // objects from the (e.g.) descendants relation of $vertex
        foreach ($indirectAndSelf as $indirectVertex) {
            $directs = [];
            foreach ($indirectVertex->$directRelation->pluck($vertex->getKeyName()) as $id) {
                $directs[] = $verticesByKey[$id];
            }
            $indirectVertex->setRelation($directRelation, $vertex->newCollection($directs));
        }
    }

    /**
     * Eager load all (e.g.) children and (e.g.) descendants of the given vertex's (e.g.) descendants. Additionally,
     * replace any vertex instance within these (e.g.) children and (e.g.) descendants arrays by the exact same instance
     * of the model, such that when recursing over the graph, whenever the same vertex is encountered, it is guaranteed
     * to be the very same object instance. Note that this is done one-way only, so only when using the (e.g.) children
     *  or (e.g.) descendants relation.
     */
    public static function linkIndirectToIndirect(Model $vertex, string $indirectRelation, string $directRelation)
    {
        // First eager load all (e.g.) children relations
        self::linkDirectToIndirect($vertex, $indirectRelation, $directRelation);

        // Having eager loaded and linked all children relations, we can now traverse over all (e.g.) descendants,
        // filling the (e.g.) descendants relation with a vertex's (e.g.) children and all the (e.g.) children's
        // descendants
        $vertex->$directRelation()->traverseDfsPostOrdering(
            function (Model $vertex) use ($indirectRelation, $directRelation) {
                $indirects = [];
                foreach ($vertex->$directRelation as $subvertex) {
                    $indirects = array_merge($indirects, [$subvertex], $subvertex->$indirectRelation->all());
                }
                $vertex->setRelation($indirectRelation, $vertex->newCollection(array_unique($indirects)));
            }
        );
    }

    /**
     * In a forest DAG, this method can be used after link(In)directToIndirect() to link vertex parents to the parent
     * relation. This way, the subtree will have two-way navigation possibilities.
     */
    public static function linkParentToDescendantChildren(Model $vertex)
    {
        if (!in_array(IsForest::class, class_uses_recursive($vertex))) {
            throw new MethodNotSupportedException('This method can only be used with forests');
        }

        $vertex->children()->traverseDfs(function (Model $subvertex) {
            foreach ($subvertex->children as $child) {
                $child->setRelation('parent', $subvertex);
            }
        });
    }
}
