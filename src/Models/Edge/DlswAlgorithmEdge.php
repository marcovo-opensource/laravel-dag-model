<?php

namespace Marcovo\LaravelDagModel\Models\Edge;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\DlswAlgorithm;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

/**
 * @api
 */
abstract class DlswAlgorithmEdge extends Pivot implements IsEdgeInDagContract
{
    use IsEdgeInDag;

    public $incrementing = true;

    /**
     * @internal
     */
    public function getTransitiveClosureAlgorithm(): TransitiveClosureAlgorithmContract
    {
        return new DlswAlgorithm($this);
    }
}
