<?php

namespace Marcovo\LaravelDagModel\Models\Edge;

use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

/**
 * @api
 */
interface IsEdgeInDagContract
{
    public const TYPE_CLOSURE_EDGE = 0;
    public const TYPE_GRAPH_EDGE = 1;
    public const TYPE_SELF_LOOP_EDGE = 2; // Only used in WithSelfLoops extension

    public function getVertexModel(): IsVertexInDagContract;

    public function getTransitiveClosureAlgorithm(): TransitiveClosureAlgorithmContract;

    public function getStartVertexColumn(): string;

    public function getEndVertexColumn(): string;

    public function getEdgeTypeColumn(): string;
}
