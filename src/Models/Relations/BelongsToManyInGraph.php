<?php

namespace Marcovo\LaravelDagModel\Models\Relations;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

class BelongsToManyInGraph extends BelongsToMany
{
    /**
     * @internal
     */
    public function __construct(
        Builder $query,
        Model $parent,
        $table,
        $foreignPivotKey,
        $relatedPivotKey,
        $parentKey,
        $relatedKey,
        $relationName,
        TransitiveClosureAlgorithmContract $transitiveClosureAlgorithm
    ) {
        parent::__construct(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName
        );

        $pivot = $transitiveClosureAlgorithm->getPivotInstance();

        $this
            ->where($pivot->qualifyColumn($pivot->getEdgeTypeColumn()), '=', IsEdgeInDagContract::TYPE_GRAPH_EDGE)
            ->using(get_class($pivot));
    }

    public function newPivotStatement()
    {
        $pivot = new $this->using();

        return parent::newPivotStatement()
            ->where($pivot->qualifyColumn($pivot->getEdgeTypeColumn()), '=', IsEdgeInDagContract::TYPE_GRAPH_EDGE);
    }

    /**
     * @internal
     */
    public function insert(array $values)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insertOrIgnore(array $values)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insertGetId(array $values, $sequence = null)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insertUsing(array $columns, $query)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function updateOrInsert(array $attributes, array $values = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function delete()
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function forceDelete()
    {
        throw new MethodNotSupportedException();
    }

    /**
     * Breadth first search
     * Performs breadth first search and stops whenever a non-null response from $callback is obtained, in which
     * case the non-null response is returned to the caller.
     *
     * @api
     */
    public function traverseBfs(\Closure $callback)
    {
        $queue = new Collection();
        $explored = new Collection();

        $queue->push($this->parent);
        $explored[$this->parent->getKey()] = true;

        while ($queue->count() > 0) {
            $vertex = $queue->shift();

            $result = $callback($vertex);
            if ($result !== null) {
                return $result;
            }

            foreach ($vertex->{$this->relationName} as $subvertex) {
                if (!$explored->has($subvertex->getKey())) {
                    $queue->push($subvertex);
                    $explored[$subvertex->getKey()] = true;
                }
            }
        }

        return null;
    }

    /**
     * Depth first search
     * Performs depth first search and stops whenever a non-null response from either callback is obtained, in which
     * case the non-null response is returned to the caller. $preOrderingCallback will be called in the pre-order
     * position of vertices ('first time going in'), $postOrderingCallback will be called in the post-order position
     * of vertices ('first time going out')
     *
     * @api
     */
    public function traverseDfs(?\Closure $preOrderingCallback = null, ?\Closure $postOrderingCallback = null)
    {
        return $this->traverseDfsStep($this->parent, $preOrderingCallback, $postOrderingCallback, new Collection());
    }

    /**
     * Depth first search, performing the given callback at post-order position of vertices
     *
     * @api
     * @see traverseDfs
     */
    public function traverseDfsPostOrdering(\Closure $postOrderingCallback)
    {
        return $this->traverseDfs(null, $postOrderingCallback);
    }

    private function traverseDfsStep(
        Model $vertex,
        ?\Closure $preOrderingCallback,
        ?\Closure $postOrderingCallback,
        Collection $visited
    ) {
        $visited[$vertex->getKey()] = true;

        if ($preOrderingCallback !== null) {
            $result = $preOrderingCallback($vertex);
            if ($result !== null) {
                return $result;
            }
        }

        foreach ($vertex->{$this->relationName} as $subvertex) {
            if (!$visited->has($subvertex->getKey())) {
                $result = $this->traverseDfsStep($subvertex, $preOrderingCallback, $postOrderingCallback, $visited);
                if ($result !== null) {
                    return $result;
                }
            }
        }

        if ($postOrderingCallback !== null) {
            $result = $postOrderingCallback($vertex);
            if ($result !== null) {
                return $result;
            }
        }

        return null;
    }
}
