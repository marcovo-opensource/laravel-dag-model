<?php

namespace Marcovo\LaravelDagModel\Models\Relations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

class BelongsToManyInTransitiveClosureWithSelfUnion extends BelongsToManyInTransitiveClosure
{
    /**
     * @internal
     * @see BelongsToMany::performJoin()
     */
    protected function performJoin($query = null)
    {
        $query = $query ?: $this->query;

        /** @var IsEdgeInDagContract|Model $edgeModel */
        $edgeModel = $this->parent->getEdgeModel();

        // Base join from pivot table
        $subQuery = $this->getConnection()->table($this->table)
            ->select([$edgeModel->getStartVertexColumn(), $edgeModel->getEndVertexColumn()]);

        // Add self-loop edges
        $subQuery->union(
            $this->getConnection()->table($this->parent->getTable())
                ->select([$this->parent->getKeyName(), $this->parent->getKeyName()])
        );

        // @codeCoverageIgnoreStart
        $key = method_exists($this, 'getQualifiedRelatedKeyName')
            ? $this->getQualifiedRelatedKeyName()
            : $this->related->getTable() . '.' . $this->relatedKey; // TODO: Remove when we drop support for Laravel < 8
        // @codeCoverageIgnoreEnd

        $query->joinSub(
            $subQuery,
            $this->table,
            $key,
            '=',
            $this->getQualifiedRelatedPivotKeyName()
        );

        return $this;
    }
}
