<?php

namespace Marcovo\LaravelDagModel\Models\Relations;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

class BelongsToManyInTransitiveClosure extends BelongsToMany
{
    /**
     * @internal
     */
    public function __construct(
        Builder $query,
        Model $parent,
        $table,
        $foreignPivotKey,
        $relatedPivotKey,
        $parentKey,
        $relatedKey,
        $relationName,
        TransitiveClosureAlgorithmContract $transitiveClosureAlgorithm
    ) {
        parent::__construct(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName
        );

        $pivot = $transitiveClosureAlgorithm->getPivotInstance();

        $this->using(get_class($pivot));
    }

    // TODO: Uncomment whenever we drop support for laravel < 9
    // public function firstOrCreate(array $attributes = [], array $values = [], array $joining = [], $touch = true)
    // {
    //     throw new MethodNotSupportedException();
    // }

    /**
     * @internal
     */
    public function updateOrCreate(array $attributes, array $values = [], array $joining = [], $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function save(Model $model, array $pivotAttributes = [], $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function saveMany($models, array $pivotAttributes = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function create(array $attributes = [], array $joining = [], $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function createMany(iterable $records, array $joinings = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function toggle($ids, $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function sync($ids, $detaching = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @api
     */
    public function updateExistingPivot($id, array $attributes, $touch = true)
    {
        foreach ($this->parent->getConfiguredTransitiveClosureAlgorithm()->getManagedColumns() as $managedColumn) {
            if (array_key_exists($managedColumn, $attributes)) {
                throw new MethodNotSupportedException();
            }
        }

        return parent::updateExistingPivot($id, $attributes, $touch);
    }

    /**
     * @internal
     */
    public function attach($id, array $attributes = [], $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function detach($ids = null, $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insert(array $values)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insertOrIgnore(array $values)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insertGetId(array $values, $sequence = null)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insertUsing(array $columns, $query)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function updateOrInsert(array $attributes, array $values = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function delete()
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function forceDelete()
    {
        throw new MethodNotSupportedException();
    }
}
