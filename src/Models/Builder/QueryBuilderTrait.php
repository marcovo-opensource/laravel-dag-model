<?php

namespace Marcovo\LaravelDagModel\Models\Builder;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;

/**
 * @api
 */
trait QueryBuilderTrait
{
    /**
     * @internal
     */
    protected function addHasRelationOrSelf(string $relation, $id, bool $orSelf, string $boolean)
    {
        $model = $this->getModel();
        if ($id instanceof $model) {
            $id = $id->getKey();
        } elseif (!is_scalar($id)) {
            throw new LaravelDagModelException('Received invalid id');
        }

        $this->where(function (Builder $query) use ($relation, $id, $orSelf) {
            $query->whereHas($relation, function (Builder $query) use ($id) {
                $query->whereKey($id);
            });

            if ($orSelf) {
                $query->orWhere($this->model->getQualifiedKeyName(), '=', $id);
            }
        }, null, null, $boolean);

        return $this;
    }

    /**
     * @api
     */
    public function whereAncestorOf($id)
    {
        return $this->addHasRelationOrSelf('descendants', $id, false, 'and');
    }

    /**
     * @api
     */
    public function orWhereAncestorOf($id)
    {
        return $this->addHasRelationOrSelf('descendants', $id, false, 'or');
    }

    /**
     * @api
     */
    public function whereAncestorOfOrSelf($id)
    {
        return $this->addHasRelationOrSelf('descendants', $id, true, 'and');
    }

    /**
     * @api
     */
    public function orWhereAncestorOfOrSelf($id)
    {
        return $this->addHasRelationOrSelf('descendants', $id, true, 'or');
    }

    /**
     * @api
     */
    public function whereDescendantOf($id)
    {
        return $this->addHasRelationOrSelf('ancestors', $id, false, 'and');
    }

    /**
     * @api
     */
    public function orWhereDescendantOf($id)
    {
        return $this->addHasRelationOrSelf('ancestors', $id, false, 'or');
    }

    /**
     * @api
     */
    public function whereDescendantOfOrSelf($id)
    {
        return $this->addHasRelationOrSelf('ancestors', $id, true, 'and');
    }

    /**
     * @api
     */
    public function orWhereDescendantOfOrSelf($id)
    {
        return $this->addHasRelationOrSelf('ancestors', $id, true, 'or');
    }
}
