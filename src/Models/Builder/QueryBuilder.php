<?php

namespace Marcovo\LaravelDagModel\Models\Builder;

use Illuminate\Database\Eloquent\Builder;

/**
 * @api
 */
class QueryBuilder extends Builder
{
    use QueryBuilderTrait;
}
