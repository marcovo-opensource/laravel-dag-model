<?php

namespace Marcovo\LaravelDagModel\Models\Extensions;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManyInTransitiveClosure;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManyInTransitiveClosureWithoutSelfLoops;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

/**
 * @mixin DagVertexModel
 */
trait WithSelfLoops
{
    public static function bootWithSelfLoops()
    {
        static::created(function (self $vertex) {
            $vertex->getConfiguredTransitiveClosureAlgorithm()->createSelfLoopEdge($vertex->getKey());
        });

        static::deleting(function (self $vertex) {
            $vertex->getConfiguredTransitiveClosureAlgorithm()->deleteSelfLoopEdge($vertex->getKey());
        });
    }

    /**
     * @api
     */
    public function rebuildSelfLoops()
    {
        $this->getConnection()->transaction(function () {
            $pivot = $this->getEdgeModel();

            // Delete any loop edges with start != end
            $edgeQuery = $pivot
                ->newQuery()
                ->where($pivot->getEdgeTypeColumn(), '=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE)
                ->whereColumn($pivot->getStartVertexColumn(), '!=', $pivot->getEndVertexColumn());

            $this->applySeparationConditionToEdgesQuery(
                $edgeQuery,
                $pivot->getTable(),
                $pivot->getStartVertexColumn()
            );

            $edgeQuery->delete();

            // Add any missing loop edges
            static::newSeparatedQuery()
                ->whereNotIn(
                    'id',
                    $pivot
                        ->newQuery()
                        ->where($pivot->getEdgeTypeColumn(), '=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE)
                        ->whereColumn($pivot->getStartVertexColumn(), $pivot->getEndVertexColumn())
                        ->tap(fn(EloquentBuilder $query) => $this->applySeparationConditionToEdgesQuery(
                            $query,
                            $pivot->getTable(),
                            $pivot->getStartVertexColumn()
                        ))
                        ->select($pivot->getStartVertexColumn())
                )
                ->chunkById(50, function ($vertices) {
                    foreach ($vertices as $vertex) {
                        $vertex->getConfiguredTransitiveClosureAlgorithm()->createSelfLoopEdge($vertex->getKey());
                    }
                });
        });
    }

    /**
     * @internal
     */
    protected function newBelongsToManyInTransitiveClosure(
        EloquentBuilder $query,
        Model $parent,
        string $table,
        string $foreignPivotKey,
        string $relatedPivotKey,
        string $parentKey,
        string $relatedKey,
        TransitiveClosureAlgorithmContract $algorithm,
        ?string $relationName = null
    ): BelongsToManyInTransitiveClosure {
        return new BelongsToManyInTransitiveClosureWithoutSelfLoops(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName,
            $algorithm
        );
    }

    /**
     * @internal
     */
    protected function newBelongsToManyInTransitiveClosureWithSelf(
        EloquentBuilder $query,
        Model $parent,
        string $table,
        string $foreignPivotKey,
        string $relatedPivotKey,
        string $parentKey,
        string $relatedKey,
        TransitiveClosureAlgorithmContract $algorithm,
        ?string $relationName = null
    ): BelongsToManyInTransitiveClosure {
        return new BelongsToManyInTransitiveClosure(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName,
            $algorithm
        );
    }
}
