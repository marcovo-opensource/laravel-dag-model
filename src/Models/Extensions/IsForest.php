<?php

namespace Marcovo\LaravelDagModel\Models\Extensions;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Marcovo\LaravelDagModel\Helpers\LinkingHelper;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\IsVertexInDag;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManySiblings;
use Marcovo\LaravelDagModel\Models\Relations\HasOneParentInForest;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

/**
 * @mixin Model
 * @mixin IsVertexInDag
 */
trait IsForest
{
    use WithMaxVertexDegree;

    /**
     * @api
     */
    final public function maxInDegree(): ?int
    {
        return 1;
    }

    /**
     * @api
     */
    public function parent(): HasOneParentInForest
    {
        $pivot = $this->getEdgeModel();

        return $this->newHasOneParentInForest(
            $this->newRelatedInstance(self::class)->newQuery(),
            $this,
            new $pivot(),
            $pivot->getEndVertexColumn(),
            $this->getKeyName(),
            $this->getKeyName(),
            $pivot->getStartVertexColumn(),
            'parent'
        )->where($pivot->qualifyColumn($pivot->getEdgeTypeColumn()), '=', IsEdgeInDagContract::TYPE_GRAPH_EDGE);
    }

    /**
     * @internal
     */
    protected function newHasOneParentInForest(
        EloquentBuilder $query,
        Model $farParent,
        Model $throughParent,
        $firstKey,
        $secondKey,
        $localKey,
        $secondLocalKey,
        $relationName
    ) {
        return new HasOneParentInForest(
            $query,
            $farParent,
            $throughParent,
            $firstKey,
            $secondKey,
            $localKey,
            $secondLocalKey,
            $relationName
        );
    }

    /**
     * @api
     */
    public function siblings(): BelongsToManySiblings
    {
        $pivot = $this->getEdgeModel();
        return $this->belongsToManySiblings(false, $pivot->getStartVertexColumn(), $pivot->getEndVertexColumn());
    }

    /**
     * @api
     */
    public function siblingsAndSelf(): BelongsToManySiblings
    {
        $pivot = $this->getEdgeModel();
        return $this->belongsToManySiblings(true, $pivot->getStartVertexColumn(), $pivot->getEndVertexColumn());
    }

    /**
     * @internal
     */
    protected function belongsToManySiblings(
        bool $andSelf,
        string $foreignPivotKey,
        string $relatedPivotKey,
        ?string $parentKey = null,
        ?string $relatedKey = null,
        ?string $relation = null
    ): BelongsToManySiblings {
        if (is_null($relation)) {
            static::$manyMethods = array_unique([
                ...static::$manyMethods,
                'belongsToManySiblings',
            ]);

            $relation = $this->guessBelongsToManyRelation();
        }

        $algorithm = $this->getConfiguredTransitiveClosureAlgorithm();
        $instance = $this->newRelatedInstance(static::class);

        return $this->newBelongsToManySiblings(
            $instance->newQuery(),
            $this,
            $this->getEdgeModel()->getTable(),
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey ?: $this->getKeyName(),
            $relatedKey ?: $this->getKeyName(),
            $relation,
            $algorithm,
            $andSelf
        );
    }

    /**
     * @internal
     */
    protected function newBelongsToManySiblings(
        EloquentBuilder $query,
        Model $parent,
        string $table,
        string $foreignPivotKey,
        string $relatedPivotKey,
        string $parentKey,
        string $relatedKey,
        ?string $relationName,
        TransitiveClosureAlgorithmContract $algorithm,
        bool $andSelf
    ): BelongsToManySiblings {
        return new BelongsToManySiblings(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName,
            $algorithm,
            $andSelf
        );
    }

    /**
     * Eager load all descendants and their children, and link them within the subtree such that any two references
     * to the same vertex will refer to the exact same object.
     * Also, the parent relation is similarly filled for each of the descendants.
     *
     * @api
     */
    public function linkDescendantChildrenAndParent(): self
    {
        $this->linkDescendantChildren();

        LinkingHelper::linkParentToDescendantChildren($this);

        return $this;
    }

    /**
     * Eager load all descendants and their children and descendants, and link them within the subtree such that any
     * two references to the same vertex will refer to the exact same object.
     * Also, the parent relation is similarly filled for each of the descendants.
     *
     * @api
     */
    public function linkDescendantDescendantsAndParent(): self
    {
        $this->linkDescendantDescendants();

        LinkingHelper::linkParentToDescendantChildren($this);

        return $this;
    }

    /**
     * Eager load all ancestors and for each ancestor fill the parent relation
     *
     * @api
     */
    public function linkAncestorParent(): self
    {
        $this->linkAncestorParents();

        $this->parents()->traverseBfs(function (self $vertex) {
            $vertex->setRelation('parent', $vertex->parents->first());
        });

        return $this;
    }

    /**
     * Iterate over all ancestors of this vertex, starting with the parent
     *
     * @api
     */
    public function iterateAncestorsInTree(bool $link = true): \Generator
    {
        if ($link) {
            $this->linkAncestorParent();
        }

        $vertex = $this;

        while ($vertex->parent !== null) {
            yield $vertex->parent;
            $vertex = $vertex->parent;
        }
    }
}
