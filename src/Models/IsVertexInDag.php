<?php

namespace Marcovo\LaravelDagModel\Models;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Marcovo\LaravelDagModel\Helpers\LinkingHelper;
use Marcovo\LaravelDagModel\Models\Builder\QueryBuilder;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManyInGraph;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManyInTransitiveClosure;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManyInTransitiveClosureWithSelfUnion;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

/**
 * @mixin Model
 */
trait IsVertexInDag
{
    public static function bootIsVertexInDag()
    {
        static::deleting(function (self $vertex) {
            $vertex->children()->sync([]);
            $vertex->parents()->sync([]);
        });
    }

    /**
     * @api
     */
    public function newEloquentBuilder($query)
    {
        return new QueryBuilder($query);
    }

    /**
     * @internal
     */
    protected function belongsToManyInGraph(
        string $foreignPivotKey,
        string $relatedPivotKey,
        ?string $parentKey = null,
        ?string $relatedKey = null,
        ?string $relation = null
    ): BelongsToManyInGraph {
        if (is_null($relation)) {
            static::$manyMethods = array_unique([
                ...static::$manyMethods,
                'belongsToManyInGraph',
            ]);

            $relation = $this->guessBelongsToManyRelation();
        }

        $algorithm = $this->getConfiguredTransitiveClosureAlgorithm();
        $instance = $this->newRelatedInstance(static::class);

        return $this->newBelongsToManyInGraph(
            $instance->newQuery(),
            $this,
            $this->getEdgeModel()->getTable(),
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey ?: $this->getKeyName(),
            $relatedKey ?: $this->getKeyName(),
            $algorithm,
            $relation
        );
    }

    /**
     * @internal
     */
    protected function newBelongsToManyInGraph(
        EloquentBuilder $query,
        Model $parent,
        string $table,
        string $foreignPivotKey,
        string $relatedPivotKey,
        string $parentKey,
        string $relatedKey,
        TransitiveClosureAlgorithmContract $algorithm,
        ?string $relationName = null
    ): BelongsToManyInGraph {
        return new BelongsToManyInGraph(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName,
            $algorithm
        );
    }

    /**
     * @internal
     */
    protected function belongsToManyInTransitiveClosure(
        string $foreignPivotKey,
        string $relatedPivotKey,
        ?string $parentKey = null,
        ?string $relatedKey = null,
        ?string $relation = null
    ): BelongsToManyInTransitiveClosure {
        if (is_null($relation)) {
            static::$manyMethods = array_unique([
                ...static::$manyMethods,
                'belongsToManyInTransitiveClosure',
            ]);

            $relation = $this->guessBelongsToManyRelation();
        }

        $algorithm = $this->getConfiguredTransitiveClosureAlgorithm();
        $instance = $this->newRelatedInstance(static::class);

        return $this->newBelongsToManyInTransitiveClosure(
            $instance->newQuery(),
            $this,
            $this->getEdgeModel()->getTable(),
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey ?: $this->getKeyName(),
            $relatedKey ?: $this->getKeyName(),
            $algorithm,
            $relation
        );
    }

    /**
     * @internal
     */
    protected function newBelongsToManyInTransitiveClosure(
        EloquentBuilder $query,
        Model $parent,
        string $table,
        string $foreignPivotKey,
        string $relatedPivotKey,
        string $parentKey,
        string $relatedKey,
        TransitiveClosureAlgorithmContract $algorithm,
        ?string $relationName = null
    ): BelongsToManyInTransitiveClosure {
        return new BelongsToManyInTransitiveClosure(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName,
            $algorithm
        );
    }

    /**
     * @internal
     */
    protected function belongsToManyInTransitiveClosureWithSelf(
        string $foreignPivotKey,
        string $relatedPivotKey,
        ?string $parentKey = null,
        ?string $relatedKey = null,
        ?string $relation = null
    ): BelongsToManyInTransitiveClosure {
        if (is_null($relation)) {
            static::$manyMethods = array_unique([
                ...static::$manyMethods,
                'belongsToManyInTransitiveClosureWithSelf',
            ]);

            $relation = $this->guessBelongsToManyRelation();
        }

        $algorithm = $this->getConfiguredTransitiveClosureAlgorithm();
        $instance = $this->newRelatedInstance(static::class);

        return $this->newBelongsToManyInTransitiveClosureWithSelf(
            $instance->newQuery(),
            $this,
            $this->getEdgeModel()->getTable(),
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey ?: $this->getKeyName(),
            $relatedKey ?: $this->getKeyName(),
            $algorithm,
            $relation
        );
    }

    /**
     * @internal
     */
    protected function newBelongsToManyInTransitiveClosureWithSelf(
        EloquentBuilder $query,
        Model $parent,
        string $table,
        string $foreignPivotKey,
        string $relatedPivotKey,
        string $parentKey,
        string $relatedKey,
        TransitiveClosureAlgorithmContract $algorithm,
        ?string $relationName = null
    ): BelongsToManyInTransitiveClosure {
        return new BelongsToManyInTransitiveClosureWithSelfUnion(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName,
            $algorithm
        );
    }

    /**
     * @api
     */
    public function descendants(): BelongsToManyInTransitiveClosure
    {
        $pivot = $this->getEdgeModel();
        return $this->belongsToManyInTransitiveClosure($pivot->getStartVertexColumn(), $pivot->getEndVertexColumn());
    }

    /**
     * @api
     */
    public function ancestors(): BelongsToManyInTransitiveClosure
    {
        $pivot = $this->getEdgeModel();
        return $this->belongsToManyInTransitiveClosure($pivot->getEndVertexColumn(), $pivot->getStartVertexColumn());
    }

    /**
     * @api
     */
    public function descendantsWithSelf(): BelongsToManyInTransitiveClosure
    {
        $pivot = $this->getEdgeModel();
        return $this->belongsToManyInTransitiveClosureWithSelf(
            $pivot->getStartVertexColumn(),
            $pivot->getEndVertexColumn()
        );
    }

    /**
     * @api
     */
    public function ancestorsWithSelf(): BelongsToManyInTransitiveClosure
    {
        $pivot = $this->getEdgeModel();
        return $this->belongsToManyInTransitiveClosureWithSelf(
            $pivot->getEndVertexColumn(),
            $pivot->getStartVertexColumn()
        );
    }

    /**
     * @api
     */
    public function children(): BelongsToManyInGraph
    {
        $pivot = $this->getEdgeModel();
        return $this->belongsToManyInGraph($pivot->getStartVertexColumn(), $pivot->getEndVertexColumn());
    }

    /**
     * @api
     */
    public function parents(): BelongsToManyInGraph
    {
        $pivot = $this->getEdgeModel();
        return $this->belongsToManyInGraph($pivot->getEndVertexColumn(), $pivot->getStartVertexColumn());
    }

    /**
     * @api
     */
    public function newSeparatedQuery(): EloquentBuilder
    {
        $query = parent::newQuery();

        $separationCondition = $this->getSeparationCondition();
        if ($separationCondition !== null) {
            $query->where($separationCondition);
        }

        return $query;
    }

    /**
     * @api
     */
    public function getSeparationCondition(): ?\Closure
    {
        return null;
    }

    /**
     * @internal
     */
    public function getConfiguredTransitiveClosureAlgorithm(): TransitiveClosureAlgorithmContract
    {
        $algorithm = $this->getEdgeModel()->getTransitiveClosureAlgorithm();

        $separationCondition = $this->getSeparationCondition();
        if ($separationCondition !== null) {
            $algorithm->setSeparationCallback(
                fn (string $edgesTableAlias, string $columnName) =>
                fn (Builder $query) =>
                $this->applySeparationConditionToEdgesQuery($query, $edgesTableAlias, $columnName)
            );
        }

        return $algorithm;
    }

    /**
     * @internal
     */
    public function applySeparationConditionToEdgesQuery($query, string $edgesTableAlias, string $columnName)
    {
        $separationCondition = $this->getSeparationCondition();
        if ($separationCondition !== null) {
            $query->whereExists(
                fn($query) => $query
                    ->from($this->getTable())
                    ->whereColumn($this->getQualifiedKeyName(), '=', $edgesTableAlias . '.' . $columnName)
                    ->where($separationCondition)
            );
        }
    }

    /**
     * Rebuild the transitive closure based on the existing graph edges
     *
     * @api
     */
    public function rebuildTransitiveClosure()
    {
        $this->getConfiguredTransitiveClosureAlgorithm()->rebuild();
    }

    /**
     * Eager load all descendants and their children, and link them within the subtree such that any two references
     * to the same vertex will refer to the exact same object.
     *
     * @api
     */
    public function linkDescendantChildren(): self
    {
        LinkingHelper::linkDirectToIndirect($this, 'descendants', 'children');

        return $this;
    }

    /**
     * Eager load all descendants and their children and descendants, and link them within the subtree such that any
     * two references to the same vertex will refer to the exact same object.
     *
     * @api
     */
    public function linkDescendantDescendants(): self
    {
        LinkingHelper::linkIndirectToIndirect($this, 'descendants', 'children');

        return $this;
    }

    /**
     * Eager load all ancestors and their parents, and link them within the subtree such that any two references
     * to the same vertex will refer to the exact same object.
     *
     * @api
     */
    public function linkAncestorParents(): self
    {
        LinkingHelper::linkDirectToIndirect($this, 'ancestors', 'parents');

        return $this;
    }

    /**
     * Eager load all ancestors and their parents and ancestors, and link them within the subtree such that any
     * two references to the same vertex will refer to the exact same object.
     *
     * @api
     */
    public function linkAncestorAncestors(): self
    {
        LinkingHelper::linkIndirectToIndirect($this, 'ancestors', 'parents');

        return $this;
    }

    /**
     * @api
     */
    public function queryIsAncestorOf(self $other): bool
    {
        return $this->descendants()->whereKey($other->getKey())->exists();
    }

    /**
     * @api
     */
    public function queryIsDescendantOf(self $other): bool
    {
        return $this->ancestors()->whereKey($other->getKey())->exists();
    }

    /**
     * @api
     */
    public function queryIsParentOf(self $other): bool
    {
        return $this->children()->whereKey($other->getKey())->exists();
    }

    /**
     * @api
     */
    public function queryIsChildOf(self $other): bool
    {
        return $this->parents()->whereKey($other->getKey())->exists();
    }
}
