<?php

namespace Marcovo\LaravelDagModel\TransitiveClosureAlgorithms;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use Illuminate\Database\Query\Grammars\SQLiteGrammar;
use Illuminate\Database\Schema\Blueprint;
use Marcovo\LaravelDagModel\Exceptions\CycleException;
use Marcovo\LaravelDagModel\Exceptions\DuplicateEdgeException;
use Marcovo\LaravelDagModel\Exceptions\EdgeNotFoundException;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Marcovo\LaravelDagModel\Exceptions\SelfLinkException;
use Marcovo\LaravelDagModel\Exceptions\UnsupportedDatabaseDriverException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * Transitive Closure computation according to algorithms in chapter 2
 * of the paper by Dong, Libkin, Su & Wong:
 * https://homepages.inf.ed.ac.uk/libkin/papers/tc-sql.pdf
 *
 * @internal
 */
class DlswAlgorithm extends AbstractTransitiveClosureAlgorithm implements TransitiveClosureAlgorithmContract
{
    public function getManagedColumns(): array
    {
        return [
            $this->pivot->getStartVertexColumn(),
            $this->pivot->getEndVertexColumn(),
            $this->pivot->getEdgeTypeColumn(),
        ];
    }

    public function getAlgorithmSpecificColumns(): array
    {
        return [
            $this->pivot->getEdgeTypeColumn(),
        ];
    }

    /**
     * @throws LaravelDagModelException
     */
    public function createEdge($startVertex, $endVertex)
    {
        $this->connection->transaction(function () use ($startVertex, $endVertex) {
            if ($startVertex === $endVertex) {
                throw new SelfLinkException();
            }

            if ($this->separationCallback !== null) {
                $this->checkSeparation($startVertex, $endVertex);
            }

            /**
             * We do not allow an edge to be created twice. Note that an edge may be present already
             * in the transitive closure, if this is the case we do allow adding the edge, and said
             * edge will be promoted to a graph edge.
             */
            if ($this->rebuildThreshold === null && $this->hasGraphEdge($startVertex, $endVertex)) {
                throw new DuplicateEdgeException();
            }

            /**
             * Cycles are not allowed in an acyclic graph
             */
            if ($this->hasEdge($endVertex, $startVertex)) {
                throw new CycleException();
            }

            /**
             * Insert the graph edge if it does not exist yet (it may exist in the TC). We will update
             * the edge_type field later.
             */
            $now = Carbon::now()->toDateTimeString();
            $this->connection->table($this->edgesTable)->insertOrIgnore([
                $this->pivot->getStartVertexColumn() => $startVertex,
                $this->pivot->getEndVertexColumn() => $endVertex,
                $this->pivot->getEdgeTypeColumn() => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                $this->pivot->getCreatedAtColumn() => $now,
                $this->pivot->getUpdatedAtColumn() => $now,
            ]);

            /**
             * Create the transitive closure edges
             */
            $this->createTcEdgesExtendingFromStart($startVertex, $endVertex);
            $this->createTcEdgesExtendingFromEnd($startVertex, $endVertex);
            $this->createTcEdgesExtendingFromStartAndEnd($startVertex, $endVertex);

            /**
             * Update the graph edge with the fact it is a graph edge
             */
            $this->connection->table($this->edgesTable)
                ->where($this->pivot->getStartVertexColumn(), '=', $startVertex)
                ->where($this->pivot->getEndVertexColumn(), '=', $endVertex)
                ->update([
                    $this->pivot->getEdgeTypeColumn() => IsEdgeInDagContract::TYPE_GRAPH_EDGE,
                    $this->pivot->getUpdatedAtColumn() => $now,
                ]);
        });
    }

    /**
     * @throws LaravelDagModelException
     */
    public function deleteEdge($startVertex, $endVertex)
    {
        $grammar = $this->connection->getQueryGrammar();

        $suspectTable = $this->edgesTable . '_suspect';
        $trustyTable = $this->edgesTable . '_trusty';
        $tcnewTable = $this->edgesTable . '_tcnew';
        $quotedSuspectTable = $grammar->wrap($suspectTable);
        $quotedTrustyTable = $grammar->wrap($trustyTable);
        $quotedTcnewTable = $grammar->wrap($tcnewTable);

        try {
            $quotedEdgesTable = $grammar->wrap($this->edgesTable);
            $startVertexName = $this->pivot->getStartVertexColumn();
            $endVertexName = $this->pivot->getEndVertexColumn();

            foreach ([$suspectTable, $trustyTable, $tcnewTable] as $tableName) {
                $this->connection->statement('
                    CREATE TEMPORARY TABLE ' . $grammar->wrap($tableName) . ' AS SELECT * FROM (
                        SELECT ' . $grammar->wrap($startVertexName) . ', ' . $grammar->wrap($endVertexName) . '
                        FROM ' . $quotedEdgesTable . '
                        LIMIT 0
                    ) AS x
                ');

                if (! $grammar instanceof MySqlGrammar) {
                    $this->connection->getSchemaBuilder()->table(
                        $tableName,
                        function (Blueprint $table) use ($startVertexName, $endVertexName) {
                            $table->unique([$startVertexName, $endVertexName]);
                            $table->index($endVertexName);
                        }
                    );
                }
            }

            $this->connection->transaction(function () use (
                $startVertex,
                $endVertex,
                $grammar,
                $startVertexName,
                $endVertexName,
                $quotedEdgesTable,
                $suspectTable,
                $trustyTable,
                $tcnewTable,
                $quotedSuspectTable,
                $quotedTrustyTable,
                $quotedTcnewTable
            ) {
                /**
                 * Check that the edge to be deleted exists, and is a graph edge
                 */
                $edge = $this->getGraphEdge($startVertex, $endVertex);
                if ($edge === null) {
                    throw new EdgeNotFoundException();
                }

                /**
                 * Update the graph edge with the fact it is not a graph edge anymore
                 */
                $keyName = $this->pivot->getKeyName();
                $this->connection->table($this->edgesTable)
                    ->where($keyName, '=', $edge->$keyName)
                    ->update([
                        $this->pivot->getEdgeTypeColumn() => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                        $this->pivot->getUpdatedAtColumn() => Carbon::now()->toDateTimeString(),
                    ]);

                /**
                 * Update transitive closure using the algorithm queries from DLSW
                 */
                $edgeTypeName = $this->pivot->getEdgeTypeColumn();

                /**
                 * Build SUSPECT table
                 */
                $query = $this->connection->query()
                    ->select('x.' . $startVertexName, 'y.' . $endVertexName)
                    ->fromRaw($quotedEdgesTable . ' as x, ' . $quotedEdgesTable . ' as y')
                    ->where('x.' . $endVertexName, '=', $startVertex)
                    ->where('y.' . $startVertexName, '=', $endVertex)
                    ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                        ->where('x.' . $edgeTypeName, '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE)
                        ->where('y.' . $edgeTypeName, '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE))
                ;

                $query->union($this->connection->query()
                    ->select('x.' . $startVertexName, $this->connection->raw($this->quoteVertexKey($endVertex)))
                    ->from($this->edgesTable . ' as x')
                    ->where('x.' . $endVertexName, '=', $startVertex)
                    ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                        ->where('x.' . $edgeTypeName, '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE)))
                ;

                $query->union($this->connection->query()
                    ->select($this->connection->raw($this->quoteVertexKey($startVertex)), 'x.' . $endVertexName)
                    ->from($this->edgesTable . ' as x')
                    ->where('x.' . $startVertexName, '=', $endVertex)
                    ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                        ->where('x.' . $edgeTypeName, '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE)))
                ;

                $query->union($this->connection->query()
                    ->select(
                        $this->connection->raw($this->quoteVertexKey($startVertex)),
                        $this->connection->raw($this->quoteVertexKey($endVertex))
                    )
                    ->from($this->edgesTable . ' as x')
                    ->where('x.' . $startVertexName, '=', $startVertex)
                    ->where('x.' . $endVertexName, '=', $endVertex));

                $this->connection->statement('
                    INSERT INTO ' . $quotedSuspectTable . ' (
                        ' . $grammar->wrap($startVertexName) . ',
                        ' . $grammar->wrap($endVertexName) . '
                    ) SELECT * FROM (
                        ' . $query->toSql() . '
                    ) AS x
                ', $query->getBindings());

                /**
                 * Build TRUSTY table
                 */
                $query = $this->selectEdgesQuery()
                    ->select($startVertexName, $endVertexName)
                    ->whereNotExists(function (Builder $query) use ($suspectTable, $startVertexName, $endVertexName) {
                        $query
                            ->select('*')
                            ->from($suspectTable)
                            ->whereColumn(
                                $suspectTable . '.' . $startVertexName,
                                '=',
                                $this->edgesTable . '.' . $startVertexName
                            )
                            ->whereColumn(
                                $suspectTable . '.' . $endVertexName,
                                '=',
                                $this->edgesTable . '.' . $endVertexName
                            );
                    })
                    ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                        ->where($this->pivot->getEdgeTypeColumn(), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE))
                ;

                $query->union($this->selectEdgesQuery()
                    ->select($startVertexName, $endVertexName)
                    ->where($this->pivot->getEdgeTypeColumn(), '=', IsEdgeInDagContract::TYPE_GRAPH_EDGE));

                $this->connection->statement('
                    INSERT INTO ' . $quotedTrustyTable . ' (
                        ' . $grammar->wrap($startVertexName) . ',
                        ' . $grammar->wrap($endVertexName) . '
                    ) SELECT * FROM (
                        ' . $query->toSql() . '
                    ) AS x
                ', $query->getBindings());

                /**
                 * Build TCNEW table
                 */
                $query = $this->connection->query()
                    ->select($startVertexName, $endVertexName)
                    ->from($trustyTable);

                $query->union($this->connection->query()
                    ->select('t1.' . $startVertexName, 't2.' . $endVertexName)
                    ->fromRaw($quotedTrustyTable . ' t1, ' . $quotedTrustyTable . ' t2')
                    ->whereColumn('t1.' . $endVertexName, '=', 't2.' . $startVertexName));

                $query->union($this->connection->query()
                    ->select('t1.' . $startVertexName, 't3.' . $endVertexName)
                    ->fromRaw($quotedTrustyTable . ' t1, ' . $quotedTrustyTable . ' t2, ' . $quotedTrustyTable . ' t3')
                    ->whereColumn('t1.' . $endVertexName, '=', 't2.' . $startVertexName)
                    ->whereColumn('t2.' . $endVertexName, '=', 't3.' . $startVertexName));

                $this->connection->statement('
                    INSERT INTO ' . $quotedTcnewTable . ' (
                        ' . $grammar->wrap($startVertexName) . ',
                        ' . $grammar->wrap($endVertexName) . '
                    ) SELECT * FROM (
                        ' . $query->toSql() . '
                    ) AS x
                ', $query->getBindings());

                /**
                 * Update the transitive closure
                 */
                $this->selectEdgesQuery()
                    ->whereNotExists(function (Builder $query) use ($tcnewTable, $startVertexName, $endVertexName) {
                        $query
                            ->select('*')
                            ->from($tcnewTable)
                            ->whereColumn(
                                $tcnewTable . '.' . $startVertexName,
                                '=',
                                $this->edgesTable . '.' . $startVertexName
                            )
                            ->whereColumn(
                                $tcnewTable . '.' . $endVertexName,
                                '=',
                                $this->edgesTable . '.' . $endVertexName
                            );
                    })
                    ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                        ->where($edgeTypeName, '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE))
                    ->delete();
            });
        } finally {
            if ($grammar instanceof MySqlGrammar) {
                $this->connection->statement('DROP TEMPORARY TABLE IF EXISTS ' . $quotedSuspectTable);
                $this->connection->statement('DROP TEMPORARY TABLE IF EXISTS ' . $quotedTrustyTable);
                $this->connection->statement('DROP TEMPORARY TABLE IF EXISTS ' . $quotedTcnewTable);
            } elseif ($grammar instanceof PostgresGrammar) {
                $this->connection->statement('DROP TABLE IF EXISTS ' . $quotedSuspectTable);
                $this->connection->statement('DROP TABLE IF EXISTS ' . $quotedTrustyTable);
                $this->connection->statement('DROP TABLE IF EXISTS ' . $quotedTcnewTable);
            } elseif ($grammar instanceof SQLiteGrammar) {
                $this->connection->statement('DROP TABLE IF EXISTS ' . $quotedSuspectTable);
                $this->connection->statement('DROP TABLE IF EXISTS ' . $quotedTrustyTable);
                $this->connection->statement('DROP TABLE IF EXISTS ' . $quotedTcnewTable);
            } else {
                throw new UnsupportedDatabaseDriverException(); // @codeCoverageIgnore
            }
        }
    }
}
