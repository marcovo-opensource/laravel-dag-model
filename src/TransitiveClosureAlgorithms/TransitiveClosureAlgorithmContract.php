<?php

namespace Marcovo\LaravelDagModel\TransitiveClosureAlgorithms;

use Illuminate\Database\Eloquent\Model;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * @internal
 */
interface TransitiveClosureAlgorithmContract
{
    public function setSeparationCallback(\Closure $separationCallback);

    /**
     * @return Model|IsEdgeInDagContract
     */
    public function getPivotInstance(): IsEdgeInDagContract;

    public function copy(): self;

    public function rebuild();

    public function beforeRebuild();

    public function getManagedColumns(): array;

    public function getAlgorithmSpecificColumns(): array;

    public function createEdge($startVertex, $endVertex);

    public function deleteEdge($startVertex, $endVertex);

    public function hasEdge($startVertex, $endVertex);

    public function hasGraphEdge($startVertex, $endVertex);

    public function getEdge($startVertex, $endVertex);

    public function getGraphEdge($startVertex, $endVertex);

    public function createSelfLoopEdge($vertex, array $extra = []): void;

    public function deleteSelfLoopEdge($vertex): void;
}
