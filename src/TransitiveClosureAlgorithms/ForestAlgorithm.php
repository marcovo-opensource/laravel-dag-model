<?php

namespace Marcovo\LaravelDagModel\TransitiveClosureAlgorithms;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use Illuminate\Database\Query\Grammars\SQLiteGrammar;
use Marcovo\LaravelDagModel\Exceptions\CycleException;
use Marcovo\LaravelDagModel\Exceptions\DuplicateEdgeException;
use Marcovo\LaravelDagModel\Exceptions\EdgeNotFoundException;
use Marcovo\LaravelDagModel\Exceptions\InDegreeException;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Marcovo\LaravelDagModel\Exceptions\SelfLinkException;
use Marcovo\LaravelDagModel\Exceptions\UnsupportedDatabaseDriverException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * @internal
 */
class ForestAlgorithm extends AbstractTransitiveClosureAlgorithm implements TransitiveClosureAlgorithmContract
{
    public function getManagedColumns(): array
    {
        return [
            $this->pivot->getStartVertexColumn(),
            $this->pivot->getEndVertexColumn(),
            $this->pivot->getEdgeTypeColumn(),
        ];
    }

    public function getAlgorithmSpecificColumns(): array
    {
        return [
            $this->pivot->getEdgeTypeColumn(),
        ];
    }

    /**
     * @throws LaravelDagModelException
     */
    public function createEdge($startVertex, $endVertex)
    {
        $this->connection->transaction(function () use ($startVertex, $endVertex) {
            if ($startVertex === $endVertex) {
                throw new SelfLinkException();
            }

            if ($this->separationCallback !== null) {
                $this->checkSeparation($startVertex, $endVertex);
            }

            /**
             * We do not allow an edge to be created twice. In a general DAG, an edge that exists
             * in the transitive closure could be promoted into a graph edge. In a forest, this would
             * imply the child would end up with more than one parent. Hence, edges cannot be promoted.
             *
             * The ORDER clause makes sure the DuplicateEdgeException takes precedence over the IndegreeException
             */
            if ($this->rebuildThreshold === null) {
                $existingEdge = $this->selectEdgesQuery()
                    ->where($this->pivot->getEndVertexColumn(), '=', $endVertex)
                    ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                        ->where($this->pivot->getEdgeTypeColumn(), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE))
                    ->orderByRaw(
                        $this->connection->getQueryGrammar()->wrap($this->pivot->getEdgeTypeColumn()) . ' = ? DESC',
                        IsEdgeInDagContract::TYPE_GRAPH_EDGE
                    )
                    ->first();

                if ($existingEdge !== null) {
                    $existingStartVertex = $existingEdge->{$this->pivot->getStartVertexColumn()};

                    if ($this->pivot->getVertexModel()->getKeyType() === 'int') {
                        $existingStartVertex = (int)$existingStartVertex;
                    }

                    if ($existingStartVertex === $startVertex) {
                        throw new DuplicateEdgeException();
                    } else {
                        throw InDegreeException::make(1);
                    }
                }
            }

            /**
             * Cycles are not allowed in an acyclic graph
             */
            if ($this->hasEdge($endVertex, $startVertex)) {
                throw new CycleException();
            }

            /**
             * Insert the graph edge
             */
            $now = Carbon::now()->toDateTimeString();
            $method = ($this->rebuildThreshold === null) ? 'insert' : 'insertOrIgnore';
            $this->connection->table($this->edgesTable)->$method([
                $this->pivot->getStartVertexColumn() => $startVertex,
                $this->pivot->getEndVertexColumn() => $endVertex,
                $this->pivot->getEdgeTypeColumn() => IsEdgeInDagContract::TYPE_GRAPH_EDGE,
                $this->pivot->getCreatedAtColumn() => $now,
                $this->pivot->getUpdatedAtColumn() => $now,
            ]);

            /**
             * Create the transitive closure edges
             */
            $this->createTcEdgesExtendingFromStart($startVertex, $endVertex);
            $this->createTcEdgesExtendingFromEnd($startVertex, $endVertex);
            $this->createTcEdgesExtendingFromStartAndEnd($startVertex, $endVertex);

            /**
             * In case we are rebuilding the TC, update the updated_at column
             */
            if ($this->rebuildThreshold !== null) {
                $this->connection->table($this->edgesTable)
                    ->where($this->pivot->getStartVertexColumn(), '=', $startVertex)
                    ->where($this->pivot->getEndVertexColumn(), '=', $endVertex)
                    ->update([
                        $this->pivot->getUpdatedAtColumn() => $now,
                    ]);
            }
        });
    }

    /**
     * @throws LaravelDagModelException
     */
    public function deleteEdge($startVertex, $endVertex)
    {
        $this->connection->transaction(function () use ($startVertex, $endVertex) {
            /**
             * Check that the edge to be deleted exists, and is a graph edge
             */
            $edge = $this->getGraphEdge($startVertex, $endVertex);
            if ($edge === null) {
                throw new EdgeNotFoundException();
            }

            /**
             * Delete all edges in the transitive closure that are implied by the edge to be deleted
             */
            $this->deleteImpliedTcEdges($startVertex, $endVertex);

            /**
             * Delete the graph edge
             */
            $keyName = $this->pivot->getKeyName();
            $this->connection->table($this->edgesTable)
                ->where($keyName, '=', $edge->$keyName)
                ->delete();
        });
    }

    private function deleteImpliedTcEdges($startVertex, $endVertex)
    {
        $grammar = $this->connection->getQueryGrammar();

        $quotedEdgesTable = $grammar->wrap($this->edgesTable);

        $wrappedStartVertexName = $grammar->wrap($this->pivot->getStartVertexColumn());
        $wrappedEndVertexName = $grammar->wrap($this->pivot->getEndVertexColumn());
        $wrappedEdgeTypeName = $grammar->wrap($this->pivot->getEdgeTypeColumn());

        $quotedStartVertexValue = $this->quoteVertexKey($startVertex);
        $quotedEndVertexValue = $this->quoteVertexKey($endVertex);

        $whereNotSelfLoopB = $whereNotSelfLoopC = '';
        if ($this->hasSelfLoops()) {
            $whereNotSelfLoopB = ' AND b.' . $wrappedEdgeTypeName . ' != ' . IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE;
            $whereNotSelfLoopC = ' AND c.' . $wrappedEdgeTypeName . ' != ' . IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE;
        }

        $this->deleteImpliedTcEdgesQuery(
            $quotedEdgesTable,
            $quotedEdgesTable . ' b',
            '
                b.' . $wrappedEndVertexName . ' = ' . $quotedStartVertexValue . '
                AND ' . $quotedEdgesTable . '.' . $wrappedStartVertexName . ' = b.' . $wrappedStartVertexName . '
                AND ' . $quotedEdgesTable . '.' . $wrappedEndVertexName . ' = ' . $quotedEndVertexValue . '
            ' . $whereNotSelfLoopB
        );

        $this->deleteImpliedTcEdgesQuery(
            $quotedEdgesTable,
            $quotedEdgesTable . ' b',
            '
                b.' . $wrappedStartVertexName . ' = ' . $quotedEndVertexValue . '
                AND ' . $quotedEdgesTable . '.' . $wrappedEndVertexName . ' = b.' . $wrappedEndVertexName . '
                AND ' . $quotedEdgesTable . '.' . $wrappedStartVertexName . ' = ' . $quotedStartVertexValue . '
            ' . $whereNotSelfLoopB
        );

        $this->deleteImpliedTcEdgesQuery(
            $quotedEdgesTable,
            $quotedEdgesTable . ' b, ' . $quotedEdgesTable . ' c',
            '
                b.' . $wrappedEndVertexName . ' = ' . $quotedStartVertexValue . '
                AND c.' . $wrappedStartVertexName . ' = ' . $quotedEndVertexValue . '
                AND ' . $quotedEdgesTable . '.' . $wrappedStartVertexName . ' = b.' . $wrappedStartVertexName . '
                AND ' . $quotedEdgesTable . '.' . $wrappedEndVertexName . ' = c.' . $wrappedEndVertexName . '
            ' . $whereNotSelfLoopB . $whereNotSelfLoopC
        );
    }

    private function deleteImpliedTcEdgesQuery(string $modifyTable, string $otherTables, string $where)
    {
        $grammar = $this->connection->getQueryGrammar();

        if ($grammar instanceof MySqlGrammar) {
            $this->connection->statement('
                DELETE FROM ' . $modifyTable . '
                USING ' . $modifyTable . ', ' . $otherTables . '
                WHERE ' . $where . '
            ');
        } elseif ($grammar instanceof PostgresGrammar) {
            $this->connection->statement('
                DELETE FROM ' . $modifyTable . '
                USING ' . $otherTables . '
                WHERE ' . $where . '
            ');
        } elseif ($grammar instanceof SQLiteGrammar) {
            $this->connection->statement('
                DELETE FROM ' . $modifyTable . '
                WHERE ROWID IN (
                    SELECT a.ROWID FROM ' . $modifyTable . ' a, ' . $otherTables . '
                    WHERE ' . $where . '
                )
            ');
        } else {
            throw new UnsupportedDatabaseDriverException(); // @codeCoverageIgnore
        }
    }
}
