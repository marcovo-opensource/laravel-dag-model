<?php

namespace Marcovo\LaravelDagModel\Exceptions;

use Throwable;

/**
 * @api
 */
class CycleException extends LaravelDagModelException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if ($message === '') {
            $message = 'The requested edge would create a directed cycle in the graph';
        }

        parent::__construct($message, $code, $previous);
    }
}
