<?php

namespace Marcovo\LaravelDagModel\Exceptions;

use Throwable;

/**
 * @api
 */
class EdgeNotFoundException extends LaravelDagModelException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if ($message === '') {
            $message = 'Could not find edge in the graph';
        }

        parent::__construct($message, $code, $previous);
    }
}
