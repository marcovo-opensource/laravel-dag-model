<?php

namespace Marcovo\LaravelDagModel\Exceptions;

use Throwable;

/**
 * @api
 */
class DuplicateEdgeException extends LaravelDagModelException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if ($message === '') {
            $message = 'The requested edge already exists in graph';
        }

        parent::__construct($message, $code, $previous);
    }
}
