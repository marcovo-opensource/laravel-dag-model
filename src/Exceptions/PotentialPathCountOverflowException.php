<?php

namespace Marcovo\LaravelDagModel\Exceptions;

use Throwable;

/**
 * @api
 */
class PotentialPathCountOverflowException extends LaravelDagModelException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if ($message === '') {
            $message = 'Adding new edges can potentially overflow the patch count register.'
                . ' Consider removing edges or changing to another transitive closure algorithm.';
        }

        parent::__construct($message, $code, $previous);
    }
}
