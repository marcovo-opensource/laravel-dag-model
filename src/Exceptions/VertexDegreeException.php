<?php

namespace Marcovo\LaravelDagModel\Exceptions;

/**
 * @api
 */
abstract class VertexDegreeException extends LaravelDagModelException
{
}
