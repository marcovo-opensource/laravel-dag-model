<?php

namespace Marcovo\LaravelDagModel\Exceptions;

/**
 * @api
 */
class LogicException extends LaravelDagModelException
{
}
